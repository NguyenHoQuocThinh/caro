package android.nguyenhoquocthinh.doancarop2p.Online;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/24/2018.
 */

public class BeforePlayOnline extends AppCompatActivity {

    private Socket socket;
    private boolean isNull = true;
    private Button buttonRandomFind;
    private Button buttonFriendList;
    private Button buttonPlayerInfo;
    private Button buttonHistory;
    private Button buttonExit;
    private String playerUsername;
    private String playerPassword;
    private AlertDialog dialog;
    private boolean opponentFound = false;
    private Player player;
    private int mode = -1;

    public BeforePlayOnline() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_before_game);

        try {
//            socket = IO.socket("http://192.168.1.6:3000/");
            socket = IO.socket("http://servercaro-quangtuan.1d35.starter-us-east-1.openshiftapps.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        socket.connect();

        socket.on("CreateAccoutFeedBack", CreateAccountFeedBack);
        socket.on("LoginFeedback", LoginFeedback);
        socket.on("FindRandomPlayerFeedback", FindRandomPlayerFeedback);
        socket.on("AcceptFeedback", AcceptFeedback);
        socket.on("CancelFeedback", CancelFeedback);
        socket.on("HistoryListFeedback", HistoryListFeedback);
        socket.on("GoingOfflineFeedback", GoingOfflineFeedback);
        socket.on("GetFriendsFeedback", GetFriendsFeedback);
        socket.on("ChallengeAFriendFeedback", ChallengeAFriendFeedback);
        socket.on("GetPlayerInfoFeedback", GetPlayerInfoFeedback);
        isNull = false;
        LoginDialog();
        Initialize();


        buttonRandomFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                socket.emit("FindRandomPlayer", playerUsername);
            }
        });

        buttonFriendList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = 1;
                Intent intent = new Intent(BeforePlayOnline.this, FriendList.class);
                intent.putExtra("player", player);
                BeforePlayOnline.this.startActivity(intent);
            }
        });

        buttonPlayerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                socket.emit("GetPlayerInfo", player.getUsername());
            }
        });

        buttonHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = 1;
                Intent intent = new Intent(BeforePlayOnline.this, HistoryMatch.class);
                intent.putExtra("player", player);
                BeforePlayOnline.this.startActivity(intent);
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                socket.emit("GoingOffline", player.getUsername());
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(SocketHandler.getSocket()!=null){
            socket.emit("GoingOnline", playerUsername);
        }
    }

    private void LoginDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BeforePlayOnline.this);
        View v = getLayoutInflater().inflate(R.layout.login_dialog, null);
        final EditText editTextUsername = (EditText)v.findViewById(R.id.editTextUsername);
        final EditText editTextPassword = (EditText)v.findViewById(R.id.editTextPassword);
        Button buttonExit_LoginDialog = (Button)v.findViewById(R.id.buttonExit_LoginDialog);
        Button buttonLogin = (Button)v.findViewById(R.id.buttonLogin);
        Button buttonSignIn = (Button)v.findViewById(R.id.buttonSignIn);

        builder.setView(v);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextPassword.getText().length()!=0 && editTextUsername.getText().length()!=0){
                    String hassPassword = "";
                    try {
                        hassPassword = HashingPassword(editTextPassword.getText().toString());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    String data = editTextUsername.getText() + "-" + hassPassword;
                    socket.emit("CreateAccount", data);
                }
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextPassword.getText().length()!=0 && editTextUsername.getText().length()!=0){
                    String hassPassword = "";
                    try {
                        hassPassword = HashingPassword(editTextPassword.getText().toString());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    playerUsername = editTextUsername.getText().toString();
                    playerPassword = hassPassword;
                    String data = editTextUsername.getText() + "-" + hassPassword;
                    socket.emit("Login", data);
                }
            }
        });

        buttonExit_LoginDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                BeforePlayOnline.this.finish();
            }
        });

    }

    private void Initialize() {
        buttonRandomFind = (Button)findViewById(R.id.buttonRandomFind_PlayOnline);
        buttonPlayerInfo = (Button)findViewById(R.id.buttonPlayerInfo_PlayOnline);
        buttonFriendList = (Button)findViewById(R.id.buttonFriendList_PlayOnline);
        buttonHistory = (Button)findViewById(R.id.buttonHistory_PlayOnline);
        buttonExit = (Button)findViewById(R.id.buttonExit_PlayOnline);
    }

    private Emitter.Listener CreateAccountFeedBack = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        boolean x = object.getBoolean("feedback");
                        String data;
                        if(x==true){
                            data = "Tạo tài khoản thành công";

                        }
                        else
                            data = "Tài khoản đã tồn tại";
                        Toast.makeText(BeforePlayOnline.this, data, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener LoginFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject)args[0];
                    try {
                        boolean x = object.getBoolean("feedback");
                        String data;
                        if(x==true){
                            data = "Đăng nhập thành công";
                            player = new Player(playerUsername, playerPassword);
                            SocketHandler.setSocket(socket);
                            dialog.dismiss();
                        }
                        else{
                            data = "Đăng nhập thất bại";
                            playerUsername = "";
                            playerPassword = "";
                        }
                        Toast.makeText(BeforePlayOnline.this, data, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener FindRandomPlayerFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject)args[0];
                    try {
                        String mess = object.getString("feedback");
                        //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                        playRandomMess = mess;
                        AcceptDialog(mess);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private boolean opponentAccept = false;
    private AlertDialog waitingDialog;
    private AlertDialog dialog1;
    private String playRandomMess;
    private boolean showing = false;

    public void AcceptDialog(String mess){
        //Toast.makeText(this, mess, Toast.LENGTH_SHORT).show();
        if(!((Activity)this).isFinishing()){
            if(!mess.equals("")){
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                dialog1 = builder.create();
                dialog1.setCancelable(false);
                String data[] = mess.split("-");
                player.setOpponent(data[0]);
                String annoucement = "Bạn sẽ quyết đấu với người chơi: " + data[0];
                dialog1.setMessage(annoucement);
                dialog1.setButton(DialogInterface.BUTTON_POSITIVE, "Chiến", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(opponentAccept == false){
                            if(!((Activity)BeforePlayOnline.this).isFinishing()){
                                final AlertDialog.Builder builder1 = new AlertDialog.Builder(BeforePlayOnline.this);
                                View v = getLayoutInflater().inflate(R.layout.waiting_dialog, null);
                                builder1.setView(v);
                                waitingDialog = builder1.create();
                                waitingDialog.show();
                                showing = true;
                            }
                            String mess1 = player.getUsername();
                            socket.emit("Accept", mess1);
                        }
                        else{
                            String data1[] = playRandomMess.split("-");
                            player.setOpponent(data1[0]);
                            dialog1.dismiss();
                            opponentAccept = false;
                            socket.emit("Accept", player.getUsername());


                            if(data1[1].compareTo("X")==0){
                                player.setPlayers(GameEngine.Players.playerX);
                            }else{
                                player.setPlayers(GameEngine.Players.playerO);

                            }
                            //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(BeforePlayOnline.this, GamePlay.class);
                            intent.putExtra("player", player);
                            BeforePlayOnline.this.startActivity(intent);
//                        if(!((Activity)BeforePlayOnline.this).isFinishing()){
//
//                        }
                        }
                    }
                });
                dialog1.setButton(DialogInterface.BUTTON_NEGATIVE, "Thôi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        player.setOpponent("");
                        socket.emit("Cancel", player.getUsername());
                        dialog.dismiss();
                    }
                });
                dialog1.setCancelable(false);
                dialog1.show();
            }
        };
    }

    private Emitter.Listener AcceptFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    opponentAccept = true;
                    if(showing==true){
                        showing = false;
                        opponentAccept = false;
                        waitingDialog.dismiss();
                        String data1[] = playRandomMess.split("-");
                        player.setOpponent(data1[0]);
                        if(data1[1].compareTo("X")==0){
                            player.setPlayers(GameEngine.Players.playerX);
                        }else{
                            player.setPlayers(GameEngine.Players.playerO);

                        }
                        //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(BeforePlayOnline.this, GamePlay.class);
                        intent.putExtra("player", player);
                        BeforePlayOnline.this.startActivity(intent);
                    }
                }
            });
        }
    };

    private Emitter.Listener CancelFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                        dialog1.dismiss();
                        if(showing){
                            waitingDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener HistoryListFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener GoingOfflineFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                       // Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener GetFriendsFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                       // Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener ChallengeAFriendFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {

                        String mess = object.getString("feedback");
                        playRandomMess = mess;
                        //Toast.makeText(BeforePlayOnline.this, mess, Toast.LENGTH_SHORT).show();
                        AcceptDialog(playRandomMess);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener GetPlayerInfoFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        ShowPlayerInfoDialog(mess);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    private void ShowPlayerInfoDialog(String mess) {
        if(!((Activity)this).isFinishing()){
            String data[] = mess.split("-");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View v = getLayoutInflater().inflate(R.layout.player_info, null);
            TextView textViewUsername = (TextView)v.findViewById(R.id.textViewUsername_PlayerInfo);
            TextView textViewWin = (TextView)v.findViewById(R.id.textViewWin_PlayerInfo);
            TextView textViewLose = (TextView)v.findViewById(R.id.textViewLose_PlayerInfo);
            TextView textViewWinPercentage = (TextView)v.findViewById(R.id.textViewWinPercentage);
            TextView textViewRank = (TextView)v.findViewById(R.id.textViewRank);
            ImageView imageViewCurrentRank = (ImageView)v.findViewById(R.id.imageViewCurrentRank);
            ImageView imageViewNextRank = (ImageView)v.findViewById(R.id.imageViewNextRank);
            ProgressBar progressBarExp = (ProgressBar)v.findViewById(R.id.progressBarExp);
            textViewUsername.setText(data[0]);
            textViewWin.setText("Số trận thắng: "+data[1]);
            textViewLose.setText("Số trận thua: "+data[2]);
            int win = Integer.parseInt(data[1]);
            int lose = Integer.parseInt(data[2]);
            float percentage;
            if((lose+win)==0)
                percentage = 0;
            else
                percentage = ((float)win/(lose+win))*100;
            textViewWinPercentage.setText("Tỉ lệ thắng: " + Float.toString(percentage) +"%");
            int rank = Integer.parseInt(data[3]);
            int exp = Integer.parseInt(data[4]);

            progressBarExp.setProgress(exp);

            switch (data[3]){
                case "1":
                    // đồng
                    textViewRank.setText("Rank: ĐỒNG");
                    imageViewCurrentRank.setBackgroundResource(R.drawable.small_bronze);
                    imageViewNextRank.setBackgroundResource(R.drawable.small_silver);
                    break;
                case "2":
                    // bạc
                    textViewRank.setText("Rank: BẠC");
                    imageViewCurrentRank.setBackgroundResource(R.drawable.small_silver);
                    imageViewNextRank.setBackgroundResource(R.drawable.small_gold);
                    break;
                case "3":
                    // vàng
                    textViewRank.setText("Rank: VÀNG");
                    imageViewCurrentRank.setBackgroundResource(R.drawable.small_gold);
                    imageViewNextRank.setBackgroundResource(R.drawable.small_plat);
                    break;
                case "4":
                    // bạch kim
                    textViewRank.setText("Rank: BẠCH KIM");
                    imageViewCurrentRank.setBackgroundResource(R.drawable.small_plat);
                    imageViewNextRank.setBackgroundResource(R.drawable.small_diamond);
                    break;
                case "5":
                    // kim cương
                    textViewRank.setText("Rank: KIM CƯƠNG");
                    imageViewCurrentRank.setBackgroundResource(R.drawable.small_diamond);
                    imageViewNextRank.setBackgroundResource(R.drawable.small_diamond);
                    break;
            }

            builder.setView(v);
            Dialog dialog;
            dialog = builder.create();
            dialog.show();
        }
    }

    private String HashingPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] b = md.digest();
        StringBuffer sb = new StringBuffer();
        for(byte b1 : b){
            sb.append(Integer.toHexString(b1 & 0xff).toString());
        }
        return sb.toString();
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        LoginDialog();
//    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mode != 1) {

            if(player!=null)
                socket.emit("GoingOffline", player.getUsername());
            else
                socket.emit("GoingOffline", "");
        }
    }

    public void onBackPressed() {
        //super.onBackPressed();
    }
}
