package android.nguyenhoquocthinh.doancarop2p.Offline.HardMode;

import android.content.Context;
import android.graphics.Color;
import android.nguyenhoquocthinh.doancarop2p.Offline.EndDialog;
import android.nguyenhoquocthinh.doancarop2p.Offline.GameEngine;
import android.nguyenhoquocthinh.doancarop2p.Online.Player;
import android.util.Log;
import android.widget.ImageButton;

import java.util.ArrayList;

public class HardModeGameEngine{
    protected ImageButton imageButton1;
    protected ImageButton imageButton2;

    protected Context context;
    protected final static int ROW_NUMBER = 21;
    protected final static int COLUMN_NUMBER = 15;
    protected float gridColumnSize;
    protected float gridRowSize;

    protected HardModeGameEngine.Players playersGrid[][];

    public enum Players{
        None, Players1, Players2
    };

    protected HardModeGameEngine.Players currentPlayer = null;

    private HardModeMachineEngine hardModeMachineEngine;
    private final Players beginPlayer;

    protected class point{
        int preI;
        int preJ;

        public point(int preI, int preJ) {
            this.preI = preI;
            this.preJ = preJ;
        }

        public int getPreI() {
            return preI;
        }

        public void setPreI(int preI) {
            this.preI = preI;
        }

        public int getPreJ() {
            return preJ;
        }

        public void setPreJ(int preJ) {
            this.preJ = preJ;
        }
    }

    private ArrayList<HardModeGameEngine.point> movement = new ArrayList<>();

    public HardModeGameEngine(){
        this.beginPlayer = Players.None;
    }

    public HardModeGameEngine(HardModeMachineEngine hardModeMachineEngine, Context context, HardModeGameEngine.Players p, ImageButton imageButton1, ImageButton imageButton2){
        playersGrid = new HardModeGameEngine.Players[ROW_NUMBER][COLUMN_NUMBER];

        this.imageButton1 = imageButton1;
        this.imageButton2 = imageButton2;

        for(int i = 0;i < ROW_NUMBER;i ++){
            for(int j = 0;j < COLUMN_NUMBER;j ++){
                playersGrid[i][j] = HardModeGameEngine.Players.None;
            }
        }
        this.hardModeMachineEngine = hardModeMachineEngine;
        this.context = context;
        currentPlayer = p;
        this.beginPlayer = p;
    }

    public void switchPlayer(){
        if(currentPlayer == HardModeGameEngine.Players.Players1){
            currentPlayer = HardModeGameEngine.Players.Players2;
        }
        else{
            currentPlayer = HardModeGameEngine.Players.Players1;
        }
        SwitchImageButton();
    }

    public HardModeGameEngine.Players checkEnd(){
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                HardModeGameEngine.Players p = getPlayerAt(i,j);
                if(p!= HardModeGameEngine.Players.None){
                    if(WinCondition1(i, j, p) || WinCondition2(i,j,p) || WinCondition3(i,j,p) || WinCondition4(i,j,p))
                        return p;
                }
            }
        }
        return HardModeGameEngine.Players.None;
    }

    public void StartGame(HardModeGameEngine.Players player){
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                playersGrid[i][j] = HardModeGameEngine.Players.None;
            }
        }
        currentPlayer = player;
        if(currentPlayer == HardModeGameEngine.Players.Players1){
            imageButton1.setEnabled(true);
            imageButton2.setEnabled(false);
            imageButton2.setColorFilter(Color.LTGRAY);
        }
    }


    protected boolean WinCondition1(int i, int j, HardModeGameEngine.Players p) {
        int index= i+1;
        int count = 0;
        if(p!= HardModeGameEngine.Players.None){
            while(index<ROW_NUMBER){
                if(playersGrid[index][j] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>1 && index < ROW_NUMBER - 1){
                        HardModeGameEngine.Players p1 = getPlayerAt(i-1,j);
                        HardModeGameEngine.Players p2 = getPlayerAt(index+1, j);
                        if(p1==p2 && p1 != p && p1 != HardModeGameEngine.Players.None){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    protected boolean WinCondition2(int i, int j, HardModeGameEngine.Players p){
        int index= j+1;
        int count = 0;
        if(p!= HardModeGameEngine.Players.None){
            while(index<COLUMN_NUMBER){
                if(playersGrid[i][index] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(j>1 && index < COLUMN_NUMBER - 1){
                        HardModeGameEngine.Players p1 = getPlayerAt(i, j-1);
                        HardModeGameEngine.Players p2 = getPlayerAt(i, index+1);
                        if(p1==p2 && p1 != p && p1 != HardModeGameEngine.Players.None){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    protected boolean WinCondition3(int i, int j, HardModeGameEngine.Players p){
        // 45 degrees line
        int iIndex = i+1;
        int jIndex = j+1;
        int count  = 0;
        if(p!= HardModeGameEngine.Players.None){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER){
                if(playersGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i > 0 && j > 0 && iIndex < ROW_NUMBER - 1 && jIndex < COLUMN_NUMBER - 1){
                        HardModeGameEngine.Players p1 = getPlayerAt(i-1, j-1);
                        HardModeGameEngine.Players p2 = getPlayerAt(iIndex+1, jIndex+1);
                        if(p1==p2 && p1 != p && p1 != HardModeGameEngine.Players.None){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex++;
                jIndex++;
            }
        }
        return false;
    }

    protected boolean WinCondition4(int i, int j, HardModeGameEngine.Players p){
        // 135 degrees line
        int iIndex = i-1;
        int jIndex = j+1;
        int count  = 0;
        if(p!= HardModeGameEngine.Players.None){
            while(iIndex < ROW_NUMBER && jIndex < COLUMN_NUMBER && iIndex>=0){
                if(playersGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i > 0 && j > 0 && i < ROW_NUMBER - 1 && iIndex > 0 && jIndex < COLUMN_NUMBER - 1){
                        HardModeGameEngine.Players p1 = getPlayerAt(i+1, j-1);
                        HardModeGameEngine.Players p2 = getPlayerAt(iIndex - 1, jIndex + 1);
                        if(p1==p2 && p1 != p && p1 != HardModeGameEngine.Players.None){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex--;
                jIndex++;
            }
        }
        return false;
    }

    public void addMark(int i, int j){
        if(getPlayerAt(i, j) == HardModeGameEngine.Players.None){
            movement.add(new HardModeGameEngine.point(i,j));
            playersGrid[i][j] = getCurrentPlayer();
            switchPlayer();
        }

        HardModeGameEngine.Players winner = checkEnd();

        if (winner != Players.None) {
            String winnerText = "";
            String icon = "";
            if (winner == this.beginPlayer) {
                Log.e("Caro", "Bạn thắng rồi!!!");
                winnerText = "Bạn thắng rồi!!!";
            } else {
                Log.e("Caro", "Thật tiêc, bạn thua rồi!");
                winnerText = "Thật tiếc, bạn thua rồi!";
            }
            if(beginPlayer == Players.Players1)
                icon = "X";
            else
                icon = "O";
            new EndDialog(context, winnerText, beginPlayer,this);
        }
    }

    public HardModeGameEngine.Players getPlayerAt(int i, int j){
        return playersGrid[i][j];
    }

    public float getGridRowSize() {
        return gridRowSize;
    }

    public void setGridRowSize(float gridRowSize) {
        this.gridRowSize = gridRowSize;
    }

    public float getGridColumnSize() {
        return gridColumnSize;
    }

    public void setGridColumnSize(float gridColumnSize) {
        this.gridColumnSize = gridColumnSize;
    }

    public int getRowNumber(){
        return ROW_NUMBER;
    }

    public int getColumnNumber(){
        return COLUMN_NUMBER;
    }

    public HardModeGameEngine.Players getCurrentPlayer() {
        return currentPlayer;
    }

    public HardModeGameEngine.Players[][] getPlayersGrid(){
        return this.playersGrid;
    }

    public ImageButton getImageButton1() { return imageButton1; }

    public void setImageButton1(ImageButton imageButton1) { this.imageButton1 = imageButton1; }

    public ImageButton getImageButton2() { return imageButton2; }

    public void setImageButton2(ImageButton imageButton2) { this.imageButton2 = imageButton2; }

    private void SwitchImageButton(){
        if(currentPlayer == HardModeGameEngine.Players.Players1){
            imageButton1.setEnabled(true);
            imageButton1.setColorFilter(null);
            imageButton2.setEnabled(false);
            imageButton2.setColorFilter(Color.LTGRAY);
        }else{
            imageButton2.setEnabled(true);
            imageButton2.setColorFilter(null);
            imageButton1.setEnabled(false);
            imageButton1.setColorFilter(Color.LTGRAY);
        }
    }

    public void MoveAgain(){
        if(movement.size()!=0){
            HardModeGameEngine.point p = movement.get(movement.size()-1);
            movement.remove(movement.size()-1);
            int i = p.getPreI();
            int j = p.getPreJ();
            playersGrid[i][j] = HardModeGameEngine.Players.None;

            p = movement.get(movement.size()-1);
            movement.remove(movement.size()-1);
            i = p.getPreI();
            j = p.getPreJ();
            playersGrid[i][j] = HardModeGameEngine.Players.None;
        }
    }

    public void machineMoves(){
        int p[] = hardModeMachineEngine.AI(Players.Players2, playersGrid);
        addMark(p[0], p[1]);
    }
}
