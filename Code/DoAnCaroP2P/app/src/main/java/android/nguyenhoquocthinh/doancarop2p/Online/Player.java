package android.nguyenhoquocthinh.doancarop2p.Online;

import java.io.Serializable;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/26/2018.
 */

public class Player implements Serializable {
    private String username;
    private String password;
    private String opponent;
    private GameEngine.Players players;

    public Player(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public GameEngine.Players getPlayers() {
        return players;
    }

    public void setPlayers(GameEngine.Players players) {
        this.players = players;
    }
}
