package android.nguyenhoquocthinh.doancarop2p.Offline.HardMode;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

class EvalBoard
{
    public int height, width;
    public int[][] EBoard;
    public int evaluationBoard = 0;

    public EvalBoard(int row, int column)
    {
        height = row;
        width = column;
        EBoard = new int[row + 2][column + 2];
        ResetBoard();
    }

    public void ResetBoard()
    {
        for (int r = 0; r < height + 2; r++)
            for (int c = 0; c < width + 2; c++)
                EBoard[r][c] = 0;
    }

    public Point MaxPos()
    {
        int Max = 0;
        Point p = new Point();
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (EBoard[i][j] > Max)
                {
                    p.i = i; p.j = j;
                    Max = EBoard[i][j];
                    //Log.e("Point: ", Integer.toString(Max));
                }

            }
        }
        if(Max == 0)
            return null;
        evaluationBoard = Max;

        return p;
    }
}

class Point{
    int i;
    int j;

    public Point(){
        i = 0;
        j = 0;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }
}

public class HardModeMachineEngine extends HardModeGameEngine{
    public int BoardArr[][] = new int[20][20]; //Nguoi 1 May 2 Chua 0
    int playerFlag = 2; //Biến cờ xác định máy đi hay người đi.
    int _x, _y; //Tọa độ nước cờ mà máy đi.

    private EvalBoard eBoard;

    private static int maxDepth = 6;
    private static int maxMove = 4;
    private int depth = 0;

    private boolean fWin = false;
    private int fEnd = 1;

    public Point goPoint;

    private long[] DScore = { 0,5,50,500,5000 };
    private long[] AScore = { 0 ,10, 100, 1000, 10000 };

    Point[] PCMove = new Point[maxMove+2];
    Point[] HumanMove = new Point[maxMove+2];
    Point[] WinMove = new Point[maxDepth+2];
    Point[] LoseMove = new Point[maxDepth + 2];

    public HardModeMachineEngine(){
        eBoard = new EvalBoard(ROW_NUMBER, COLUMN_NUMBER);
    }

    private void EvalChessBoard(int player, EvalBoard eBoard, Players[][] playersGrid)
    {
        int rw, cl, ePC, eHuman;
        eBoard.ResetBoard();

        //Horizontal
        for (rw = 0; rw < ROW_NUMBER; rw++)
            for (cl = 0; cl < COLUMN_NUMBER - 4; cl++)
            {
                ePC = 0; eHuman = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (playersGrid[rw][cl + i] == Players.Players1) eHuman++;
                    if (playersGrid[rw][cl + i] == Players.Players2) ePC++;
                }

                if (eHuman * ePC == 0 && eHuman != ePC)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (playersGrid[rw][cl + i] == Players.None) // Neu o chua duoc danh
                        {
                            if (eHuman == 0)
                                if (player == 1)
                                    eBoard.EBoard[rw][cl + i] += DScore[ePC];
                                    else eBoard.EBoard[rw][cl + i] += AScore[ePC];
                            if (ePC == 0)
                                if (player == 2)
                                    eBoard.EBoard[rw][cl + i] += DScore[eHuman];
                                    else eBoard.EBoard[rw][cl + i] += AScore[eHuman];
                            if (eHuman == 4 || ePC == 4)
                                eBoard.EBoard[rw][cl + i] *= 2;
                        }
                    }

                }
            }

        //Vertical
        for (cl = 0; cl < COLUMN_NUMBER; cl++)
            for (rw = 0; rw < ROW_NUMBER - 4; rw++)
            {
                ePC = 0; eHuman = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (playersGrid[rw + i][cl] == Players.Players1) eHuman++;
                    if (playersGrid[rw + i][cl] == Players.Players2) ePC++;
                }

                if (eHuman * ePC == 0 && eHuman != ePC)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (playersGrid[rw + i][cl] == Players.None) // Neu o chua duoc danh
                        {
                            if (eHuman == 0)
                                if (player == 1)
                                    eBoard.EBoard[rw + i][cl] += DScore[ePC];
                                    else eBoard.EBoard[rw + i][cl] += AScore[ePC];
                            if (ePC == 0)
                                if (player == 2)
                                    eBoard.EBoard[rw + i][cl] += DScore[eHuman];
                                    else eBoard.EBoard[rw + i][cl] += AScore[eHuman];
                            if (eHuman == 4 || ePC == 4)
                                eBoard.EBoard[rw + i][cl] *= 2;
                        }
                    }

                }
            }

        //Cross Down
        for (cl = 0; cl < COLUMN_NUMBER - 4; cl++)
            for (rw = 0; rw < ROW_NUMBER - 4; rw++)
            {
                ePC = 0; eHuman = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (playersGrid[rw + i][cl + i] == Players.Players1) eHuman++;
                    if (playersGrid[rw + i][cl + i] == Players.Players2) ePC++;
                }

                if (eHuman * ePC == 0 && eHuman != ePC)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (playersGrid[rw + i][cl + i] ==  Players.None) // Neu o chua duoc danh
                        {
                            if (eHuman == 0)
                                if (player == 1)
                                    eBoard.EBoard[rw + i][cl + i] += DScore[ePC];
                                    else eBoard.EBoard[rw + i][cl + i] += AScore[ePC];
                            if (ePC == 0)
                                if (player == 2)
                                    eBoard.EBoard[rw + i][cl + i] += DScore[eHuman];
                                    else eBoard.EBoard[rw + i][cl + i] += AScore[eHuman];
                            if (eHuman == 4 || ePC == 4)
                                eBoard.EBoard[rw + i][cl + i] *= 2;
                        }
                    }

                }
            }

        //Cross Up
        for (rw = 4; rw < ROW_NUMBER; rw++)
            for (cl = 0; cl < COLUMN_NUMBER - 4; cl++)
            {
                ePC = 0; eHuman = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (playersGrid[rw - i][cl + i] == Players.Players1) eHuman++;
                    if (playersGrid[rw - i][cl + i] == Players.Players2) ePC++;
                }

                if (eHuman * ePC == 0 && eHuman != ePC)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (playersGrid[rw - i][cl + i] == Players.None) // Neu o chua duoc danh
                        {
                            if (eHuman == 0)
                                if (player == 1)
                                    eBoard.EBoard[rw - i][cl + i] += DScore[ePC];
                                    else eBoard.EBoard[rw - i][cl + i] += AScore[ePC];
                            if (ePC == 0)
                                if (player == 2)
                                    eBoard.EBoard[rw - i][cl + i] += DScore[eHuman];
                                    else eBoard.EBoard[rw - i][cl + i] += AScore[eHuman];
                            if (eHuman == 4 || ePC == 4)
                                eBoard.EBoard[rw - i][cl + i] *= 2;
                        }
                    }

                }
            }
    }

    public void alphaBeta(int alpha, int beta, int depth, Players player, Players[][] playersGrid) {
        if(player== Players.Players2){
            maxValue(playersGrid, alpha, beta, depth);

        }else{
            minValue(playersGrid, alpha, beta, depth);
        }
    }

    private int maxValue(Players[][] playersGrid, int alpha, int beta, int depth) {

        eBoard.MaxPos();  // tinh toa do co diem cao nhat
        int value = eBoard.evaluationBoard; // gia tri max hien tai
        if (depth >= maxDepth) {
            return value;
        }
        EvalChessBoard(2, eBoard, playersGrid); // danh gia diem voi nguoi choi hien tai la PC
        ArrayList<Point> list = new ArrayList<>(); // list cac nut con
        for (int i = 0; i < maxMove; i++) {
            Point node = eBoard.MaxPos();
            if(node == null)
                break;
            list.add(node);
            eBoard.EBoard[node.i][node.j] = 0;
        }
        int v = Integer.MIN_VALUE;
        for (int i = 0; i < list.size(); i++) {
            Point com = list.get(i);
            playersGrid[com.i][com.j] = Players.Players2;
            v = Math.max(v, minValue(playersGrid, alpha, beta, depth+1));
            playersGrid[com.i][com.j] = Players.None;

            if(v >= beta || CheckEnd(com.j, com.i, playersGrid) == 2){
                goPoint = com;
                return v;

            }
            alpha = Math.max(alpha, v);
        }

        return v;
    }

    private int minValue(Players[][] playersGrid, int alpha, int beta, int depth) {

        eBoard.MaxPos();
        int value = eBoard.evaluationBoard;
        if (depth >= maxDepth) {
            return value;
        }

        ArrayList<Point> list = new ArrayList<>(); // list cac nut con
        for (int i = 0; i < maxMove; i++) {
            Point node = eBoard.MaxPos();
            if(node==null)
                break;
            list.add(node);
            eBoard.EBoard[node.i][node.j] = 0;
        }
        int v = Integer.MAX_VALUE;
        for (int i = 0; i < list.size(); i++) {
            Point com = list.get(i);
            playersGrid[com.i][com.j] = Players.Players1;

            v = Math.min(v, maxValue(playersGrid, alpha, beta, depth+1));

            playersGrid[com.i][com.j] = Players.None;
            if(v <= alpha || CheckEnd(com.j, com.i, playersGrid) == 1){
                return v;
            }
            beta = Math.min(beta, v);
        }
        return v;
    }



    // tinh toan nuoc di
    public int[] AI(Players player, Players[][] playersGrid) {
        int p[] = new int[2];
        p[0] = -1;
        p[1] = -1;

        alphaBeta(0, 1,2 ,player, playersGrid);

        Point temp = goPoint;
        if (temp != null) {
            p[0] = temp.i;
            p[1] = temp.j;
        }
        return p;
    }

    /*public int[] AI(Players[][] playersGrid)
    {
        for (int i = 0; i < maxMove; i++)
        {
            WinMove[i] = new Point();
            PCMove[i] = new Point();
            HumanMove[i] = new Point();
        }

        depth = 0;
        int p[] = new int[2];
        p = FindMove(playersGrid);

        Log.e("Possss: ", p[0] + "   " + p[1]);
        return p;
    }*/

    private int CheckEnd(int cl, int rw, Players[][] playersGrid)
    {
        int r = 0, c = 0;
        int i;
        boolean human, pc;
        //Check hàng ngang
        while (c < COLUMN_NUMBER - 4)
        {
            human = true; pc = true;
            for (i = 0; i < 5; i++)
            {
                Log.e("Horizontal Coordinate: ", rw + ", " + c + " + " + i);
                if (playersGrid[rw][c + i] != Players.Players1)
                    human = false;
                if (playersGrid[rw][c + i] != Players.Players2)
                    pc = false;
            }
            if (human) return 1;if (pc) return 2;
            c++;
        }

        //Check hàng dọc
        while (r < ROW_NUMBER - 4)
        {
            human = true; pc = true;
            for (i = 0; i < 5; i++)
            {
                Log.e("Vertical Coordinate: ", r + " + " + i + ", " + cl);
                if (playersGrid[r + i][cl] != Players.Players1)
                human = false;
                if (playersGrid[r + i][cl] != Players.Players2)
                pc = false;
            }
            if (human) return 1;
            if (pc) return 2;
            r++;
        }

        //Check duong cheo xuong
        r = rw; c = cl;
        while (r > 0 && c > 0) { r--; c--; }
        while (r < ROW_NUMBER - 4 && c < COLUMN_NUMBER - 4)
        {
            human = true; pc = true;
            for (i = 0; i < 5; i++)
            {
                Log.e("Cross Down Coordinate: ", r + " + " + i + ", " + c + " + " + i);
                if (playersGrid[r + i][c + i] != Players.Players1)
                    human = false;
                if (playersGrid[r + i][c + i] != Players.Players2)
                    pc = false;
            }
            if (human) return 1;
            if (pc) return 2;
            r++; c++;
        }

        //Check duong cheo len
        r = rw; c = cl;
        while (r < ROW_NUMBER - 1 && c > 0) { r++; c--; }
        while (r >= 4 && c < COLUMN_NUMBER - 4)
        {
            human = true; pc = true;
            for (i = 0; i < 5; i++)
            {
                Log.e("Cross Up Coordinate: ", r + "-" + i + ", " + c + " + " + i);
                if (playersGrid[r - i][c + i] != Players.Players1)
                    human = false;
                if (playersGrid[r - i][c + i] != Players.Players2)
                    pc = false;
            }
            if (human) return 1;
            if (pc) return 2;
            r--; c++;
        }
        return 0;
    }
}
