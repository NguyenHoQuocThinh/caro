package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.nguyenhoquocthinh.doancarop2p.MainActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;

import java.util.ArrayList;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/22/2018.
 */

public class GameEngine {

    private ImageButton imageButton1;
    private ImageButton imageButton2;

    private final static int GRID_NUMBER = 10;
    private final static int ROW_NUMBER = 10;
    private final static int COLUMN_NUMBER = 13;
    private float gridSize;
    private Players player1;
    private Context context;
    private Players playerGrid[][];
    private Players currentPlayer;


    private class point{
        int preI;
        int preJ;

        public point(int preI, int preJ) {
            this.preI = preI;
            this.preJ = preJ;
        }

        public int getPreI() {
            return preI;
        }

        public void setPreI(int preI) {
            this.preI = preI;
        }

        public int getPreJ() {
            return preJ;
        }

        public void setPreJ(int preJ) {
            this.preJ = preJ;
        }
    }

    private ArrayList<point> movement = new ArrayList<>();


    public enum Players{
        none, playerX, playerO
    }

    public GameEngine(Context context, Players inputPlayer, ImageButton imageButton1, ImageButton imageButton2){
        this.context = context;
        this.imageButton1 = imageButton1;
        this.imageButton2 = imageButton2;
        this.player1 = inputPlayer;
        if(inputPlayer == Players.playerX)
            currentPlayer = Players.playerX;
        else
            currentPlayer = Players.playerO;
        playerGrid = new Players[ROW_NUMBER][COLUMN_NUMBER];
        StartGame(currentPlayer);
    }

    public ImageButton getImageButton1() {
        return imageButton1;
    }

    public void setImageButton1(ImageButton imageButton1) {
        this.imageButton1 = imageButton1;
    }

    public ImageButton getImageButton2() {
        return imageButton2;
    }

    public void setImageButton2(ImageButton imageButton2) {
        this.imageButton2 = imageButton2;
    }

    public Players getPlayer1() {
        return player1;
    }

    public void setPlayer1(Players player1) {
        this.player1 = player1;
    }

    public float getGridSize() {
        return gridSize;
    }

    public void setGridSize(float gridSize) {
        this.gridSize = gridSize;
    }

    public Players getPlayerAt(int i, int j){
        return playerGrid[i][j];
    }

    public static int getGridNumber() {
        return GRID_NUMBER;
    }

    public static int getRowNumber() {
        return ROW_NUMBER;
    }

    public static int getColumnNumber() {
        return COLUMN_NUMBER;
    }

    public Players getCurrentPlayer() {
        return currentPlayer;
    }

    public void StartGame(Players player){
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                playerGrid[i][j] = Players.none;
            }
        }
        currentPlayer = player;
        if(currentPlayer == player1){
            imageButton1.setEnabled(true);
            imageButton2.setEnabled(false);
            imageButton2.setColorFilter(Color.LTGRAY);
        }
    }

    public void addMark(int i, int j) {
        if(getPlayerAt(i,j) == Players.none){
            movement.add(new point(i,j));
            playerGrid[i][j] = getCurrentPlayer();
            SwitchPlayer();
        }

        Players tmp = CheckEnd();
        if(tmp!=Players.none){
            String winner ="";
            if(tmp == player1) {
                winner = "Player1";
            }
            else{
                winner = "Player2";
            }
            new EndDialog(context,winner, player1, this );
        }

        boolean draw = true;
        // kiểm tra có hòa hay không
        for(int x = 0; x<ROW_NUMBER; x++){
            for(int y = 0; y<COLUMN_NUMBER; y++){
                Players p = getPlayerAt(x,y);
                if(p==Players.none){
                    draw = false;
                    break;
                }
            }
        }

        if(draw == true){
            String winner = "none";
            new EndDialog(context, winner, player1, this);
        }

    }

    private void SwitchPlayer() { // đổi icon khi đến lượt người chơi khác
        if(currentPlayer == Players.playerX){
            currentPlayer = Players.playerO;
        }else{
            currentPlayer = Players.playerX;
        }
        SwitchImageButton();
    }

    private void SwitchImageButton(){
        if(currentPlayer == player1){
            imageButton1.setEnabled(true);
            imageButton1.setColorFilter(null);
            imageButton2.setEnabled(false);
            imageButton2.setColorFilter(Color.LTGRAY);
        }else{
            imageButton2.setEnabled(true);
            imageButton2.setColorFilter(null);
            imageButton1.setEnabled(false);
            imageButton1.setColorFilter(Color.LTGRAY);
        }
    }

    public Players CheckEnd(){
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                Players p = getPlayerAt(i,j);
                if(p!=Players.none){
                    if(WinCondition1(i, j, p) || WinCondition2(i,j,p) || WinCondition3(i,j,p) || WinCondition4(i,j,p))
                        return p;
                }
            }
        }
        return Players.none;
    }

    private boolean WinCondition1(int i, int j, Players p) {
        int index= i+1;
        int count = 0;
        if(p!=Players.none){
            while(index<ROW_NUMBER){
                if(playerGrid[index][j] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>1&&index+1<ROW_NUMBER){
                        Players p1 = getPlayerAt(i-1,j);
                        Players p2 = getPlayerAt(index+1, j);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition2(int i, int j, Players p){
        int index= j+1;
        int count = 0;
        if(p!=Players.none){
            while(index<COLUMN_NUMBER){
                if(playerGrid[i][index] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(j>1&&index+1<COLUMN_NUMBER){
                        Players p1 = getPlayerAt(i, j-1);
                        Players p2 = getPlayerAt(i, index+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition3(int i, int j, Players p){
        // 45 degrees line
        int iIndex = i+1;
        int jIndex = j+1;
        int count  = 0;
        if(p!=Players.none){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER){
                if(playerGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>0&&j>0&&iIndex+1<ROW_NUMBER&&jIndex+1<COLUMN_NUMBER){
                        Players p1 = getPlayerAt(i-1, j-1);
                        Players p2 = getPlayerAt(iIndex+1, jIndex+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex++;
                jIndex++;
            }
        }
        return false;
    }

    private boolean WinCondition4(int i, int j, Players p){
        // 135 degrees line
        int iIndex = i-1;
        int jIndex = j+1;
        int count  = 0;
        if(p!=Players.none){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER&&iIndex>=0){
                if(playerGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(j-1>0&&i+1<ROW_NUMBER&&iIndex-1>0&&jIndex+1<COLUMN_NUMBER){
                        Players p1 = getPlayerAt(i+1, j-1);
                        Players p2 = getPlayerAt(iIndex-1, jIndex+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex--;
                jIndex++;
            }
        }
        return false;
    }

    public void MoveAgain(){
        if(movement.size()!=0){
            point p = movement.get(movement.size()-1);
            movement.remove(movement.size()-1);
            int i = p.getPreI();
            int j = p.getPreJ();
            playerGrid[i][j] = Players.none;
            SwitchPlayer();
        }
    }

}
