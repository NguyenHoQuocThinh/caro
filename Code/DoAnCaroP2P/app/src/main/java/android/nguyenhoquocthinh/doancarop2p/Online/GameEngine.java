package android.nguyenhoquocthinh.doancarop2p.Online;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/25/2018.
 */

public class GameEngine extends AppCompatActivity{
    private final static int COLUMN_NUMBER = 20;
    private final static int ROW_NUMBER = 15;
    private float gridSize;
    private Players playerIcon;
    private Context context;
    private Players playerGrid[][];
    private Players currentPlayer = Players.playerX;
    private Socket socket;
    private Player player;
    private ImageButton imageButtonX, imageButtonO;

    public void addMark(int i, int j) {
        if(getPlayerAt(i,j) == Players.none && currentPlayer == playerIcon){
            playerGrid[i][j] = getCurrentPlayer();

            SwitchPlayer();
            SwitchImageButton();
            String data = Integer.toString(i)+"-"+Integer.toString(j)+"-"+player.getUsername()+"-";
            if(getCurrentPlayer() == Players.playerX)
                data = data + "playerX";
            else
                data = data + "playerO";
            socket.emit("NewMovement", data);
        }

        Players tmp = CheckEnd();
        if(tmp!=Players.none){
            String mess = player.getUsername()+"-"+player.getOpponent()+"-win";
            socket.emit("Result", mess);
            endDialog(tmp);

        }

        // kiểm tra có hòa hay không
        boolean draw = true;
        for(int x = 0; x<ROW_NUMBER; x++){
            for(int y = 0; y<COLUMN_NUMBER; y++){
                Players p = getPlayerAt(x,y);
                if(p==Players.none){
                    draw = false;
                    break;
                }
            }
        }
        if(draw == true){
            Players tmp2 = Players.none;
            String mess = player.getUsername()+"-"+player.getOpponent()+"-draw";
            socket.emit("Result", mess);
            endDialog(tmp2);
        }
    }



    private void SwitchPlayer() {
        if(currentPlayer == Players.playerX){
            currentPlayer = Players.playerO;
        }else{
            currentPlayer = Players.playerX;
        }
    }

    public enum Players{
        none, playerX, playerO
    }

    public GameEngine(Context context, Player player, ImageButton imageButtonX, ImageButton imageButtonO){
        this.context = context;
        this.player = player;
        this.imageButtonX = imageButtonX;
        this.imageButtonO = imageButtonO;
        if(player.getPlayers() == Players.playerX)
            playerIcon = Players.playerX;
        else
            playerIcon = Players.playerO;

        socket = SocketHandler.getSocket();

        socket.on("NewMovementFeedback", NewMovementFeedback);
        socket.on("QuitFeedback", QuitFeedback);
        socket.on("ResultFeedback", ResultFeedback);

        playerGrid = new Players[ROW_NUMBER][COLUMN_NUMBER];
        StartNewGame();
        SwitchImageButton();
    }

    public Players getCurrentPlayer() {
        return currentPlayer;
    }

    public static int getColumnNumber() {
        return COLUMN_NUMBER;
    }

    public static int getRowNumber() {
        return ROW_NUMBER;
    }

    public float getGridSize() {
        return gridSize;
    }

    public Players getPlayerIcon() {
        return playerIcon;
    }

    public void setGridSize(float gridSize) {
        this.gridSize = gridSize;
    }

    private void StartNewGame() {
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                playerGrid[i][j] = Players.none;
            }
        }
        currentPlayer = Players.playerX;
    }

    public Players getPlayerAt(int i, int j){
            return playerGrid[i][j];
    }

    public Players CheckEnd(){
        for(int i = 0; i<ROW_NUMBER; i++){
            for(int j = 0; j<COLUMN_NUMBER; j++){
                Players p = getPlayerAt(i,j);
                if(p!=Players.none){
                    if(WinCondition1(i, j, p) || WinCondition2(i,j,p) || WinCondition3(i,j,p) || WinCondition4(i,j,p))
                        return p;
                }
            }
        }
        return Players.none;
    }

    private boolean WinCondition1(int i, int j, Players p) {
        int index= i+1;
        int count = 0;
        if(p!=Players.none){
            while(index<ROW_NUMBER){
                if(playerGrid[index][j] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>1){
                        Players p1 = getPlayerAt(i-1,j);
                        Players p2 = getPlayerAt(index+1, j);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition2(int i, int j, Players p){
        int index= j+1;
        int count = 0;
        if(p!=Players.none){
            while(index<COLUMN_NUMBER){
                if(playerGrid[i][index] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(j>1){
                        Players p1 = getPlayerAt(i, j-1);
                        Players p2 = getPlayerAt(i, index+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition3(int i, int j, Players p){
        // 45 degrees line
        int iIndex = i+1;
        int jIndex = j+1;
        int count  = 0;
        if(p!=Players.none){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER){
                if(playerGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>0&&j>0&&iIndex+1<ROW_NUMBER&&jIndex+1<COLUMN_NUMBER){
                        Players p1 = getPlayerAt(i-1, j-1);
                        Players p2 = getPlayerAt(iIndex+1, jIndex+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex++;
                jIndex++;
            }
        }
        return false;
    }

    private boolean WinCondition4(int i, int j, Players p){
        // 135 degrees line
        int iIndex = i-1;
        int jIndex = j+1;
        int count  = 0;
        if(p!=Players.none){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER&&iIndex>=0){
                if(playerGrid[iIndex][jIndex] != p){
                    return false;
                }else{
                    count++;
                }
                if(count == 4){
                    if(i>0&&j>0&&iIndex-1>0&&jIndex+1<COLUMN_NUMBER){
                        Players p1 = getPlayerAt(i+1, j-1);
                        Players p2 = getPlayerAt(iIndex-1, jIndex+1);
                        if(p1==p2 && p1 != p && p1 != Players.none){
                            return false;
                        }
                    }
                    return true;
                }
                iIndex--;
                jIndex++;
            }
        }
        return false;
    }

    private Emitter.Listener NewMovementFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        // cấu trúc mess là: i-j-opponent-currentplayer;
                        String data[] = mess.split("-");
                        int i = Integer.parseInt(data[0]);
                        int j = Integer.parseInt(data[1]);
                        if(data[3].compareTo("playerX")==0){
                            playerGrid[i][j] = Players.playerO;
                            currentPlayer = Players.playerX;
                        }else{
                            playerGrid[i][j] = Players.playerX;
                            currentPlayer = Players.playerO;
                        }

                        Players tmp = CheckEnd();
                        if(tmp!=Players.none){
                            endDialog(tmp);
                        }

                        boolean draw = true;
                        for(int x = 0; x<ROW_NUMBER; x++){
                            for(int y = 0; y<COLUMN_NUMBER; y++){
                                Players p = getPlayerAt(x,y);
                                if(p==Players.none){
                                    draw = false;
                                    break;
                                }
                            }
                        }
                        if(draw == true){
                            Players tmp2 = Players.none;
                            endDialog(tmp2);
                        }


                        SwitchImageButton();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener QuitFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        //Toast.makeText(context, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    public void endDialog(Players winner){
        if(!((Activity)context).isFinishing()){
            if(winner == Players.none){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("KẾT QUẢ")
                        .setMessage("Hòa")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Exit();
                                String mess = player.getUsername();
                                socket.emit("Quit", mess);
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCancelable(false);
            }
            else{
                if(winner == playerIcon){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("KẾT QUẢ")
                            .setMessage("Thắng cuộc")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Exit();
                                    String mess = player.getUsername();
                                    socket.emit("Quit", mess);
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    dialog.setCancelable(false);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("KẾT QUẢ")
                            .setMessage("Thua cuộc")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Exit();
                                    String mess = player.getUsername();
                                    socket.emit("Quit", mess);
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    dialog.setCancelable(false);
                }
            }

        }
    }

    private Emitter.Listener ResultFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                      //  Toast.makeText(context, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void Exit() {
        Activity a = (Activity)context;
        a.finish();
    }

    public void SwitchImageButton(){
        if(getCurrentPlayer() == Players.playerX){
            imageButtonX.setEnabled(true);
            imageButtonX.setColorFilter(null);
            imageButtonO.setEnabled(false);
            imageButtonO.setColorFilter(Color.LTGRAY);
        }
        else {
            imageButtonO.setEnabled(true);
            imageButtonO.setColorFilter(null);
            imageButtonX.setEnabled(false);
            imageButtonX.setColorFilter(Color.LTGRAY);
        }
    }

}
