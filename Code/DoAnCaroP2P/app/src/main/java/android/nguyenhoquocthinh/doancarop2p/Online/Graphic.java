package android.nguyenhoquocthinh.doancarop2p.Online;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/25/2018.
 */

public class Graphic extends View {
    private GameEngine gameEngine;
    private float scaleFactor = 1.0f;
    private ScaleGestureDetector scaleGestureDetector;
    private Paint mPaint;
    private Bitmap bitmapX, bitmapO;
    private float sizeOfElement;
    private float focusX;
    private float focusY;
    private int row = gameEngine.getColumnNumber();
    private int column = gameEngine.getRowNumber();

    public Graphic(Context context, GameEngine gameEngine){
        super(context);
        setFocusable(true);
        //scaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
        this.gameEngine = gameEngine;
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.scale(scaleFactor, scaleFactor);
        if(gameEngine.getGridSize() == 0){
            gameEngine.setGridSize(canvas.getWidth());
            sizeOfElement = gameEngine.getGridSize()/column;
            LoadAndCreateBitmaps();
        }
        drawGrid(canvas);
        drawPlayers(canvas);
        canvas.restore();
        invalidate();
    }

    private void LoadAndCreateBitmaps() {
        // Load bitmap
        bitmapX = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
        bitmapO = BitmapFactory.decodeResource(this.getResources(), R.drawable.o);


        // Resize bitmap
        bitmapX = Bitmap.createScaledBitmap(bitmapX, (int)sizeOfElement, (int)sizeOfElement, true);
        bitmapO = Bitmap.createScaledBitmap(bitmapO, (int)sizeOfElement, (int)sizeOfElement, true);

    }

    private void drawPlayers(Canvas canvas) {
        for(int i =0; i<column; i++){
            for(int j = 0; j<row; j++){
                if(gameEngine.getPlayerAt(i,j) != GameEngine.Players.none){
                    Bitmap tmpBitmp = null;

                    if(gameEngine.getPlayerAt(i,j) == GameEngine.Players.playerX){
                        tmpBitmp = bitmapX;
                    }

                    if(gameEngine.getPlayerAt(i,j) == GameEngine.Players.playerO){
                        tmpBitmp = bitmapO;
                    }
                    canvas.drawBitmap(tmpBitmp, sizeOfElement*i, sizeOfElement*j, mPaint);
                }
            }
        }
    }

    private void drawGrid(Canvas canvas) {
        for(int i = 0; i<row; i++){
            // vẽ hàng ngang
            canvas.drawLine(0, sizeOfElement*i, gameEngine.getGridSize(), sizeOfElement*i, mPaint);
        }
        for(int i = 0; i<column; i++){
            // vẽ hàng dọc
            canvas.drawLine(sizeOfElement*i, 0, sizeOfElement*i, gameEngine.getGridSize()+500, mPaint);
        }
    }

    private int startX, startY, stopX, stopY;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //scaleGestureDetector.onTouchEvent(event);
        float sizeOfElement = gameEngine.getGridSize()*scaleFactor/column;

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                startX = (int)(event.getX()/sizeOfElement);
                startY = (int)(event.getY()/sizeOfElement);
                break;
            case MotionEvent.ACTION_UP:
                stopX = (int)(event.getX()/sizeOfElement);
                stopY = (int)(event.getY()/sizeOfElement);
                break;
            default:
                break;
        }

        if(startX < column && startY <row){
            if(startX == stopX && startY == stopY){
                gameEngine.addMark(startX, startY);
            }
        }
        invalidate();
        return true;
    }
}
