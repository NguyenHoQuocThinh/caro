package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/22/2018.
 */

public class Graphic extends View {
    private GameEngine gameEngine;
    private float scaleFactor = 1.0f;
    private ScaleGestureDetector scaleGestureDetector;
    private Paint mPaint;
    private Bitmap bitmapX, bitmapO;
    private float sizeOfElement;
    private float focusX;
    private float focusY;

    public Graphic(Context context, GameEngine gameEngine){
        super(context);
        setFocusable(true);
        scaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
        this.gameEngine = gameEngine;
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.scale(scaleFactor, scaleFactor);

        if(gameEngine.getGridSize() == 0){
            gameEngine.setGridSize(canvas.getWidth());

            sizeOfElement = gameEngine.getGridSize()/gameEngine.getGridNumber();
            LoadAndCreateBitmaps();
        }

        drawGrid(canvas);
        drawPlayers(canvas);
        canvas.restore();
        invalidate();
    }

    private void drawPlayers(Canvas canvas) {
        for(int i =0; i<gameEngine.getGridNumber(); i++){
            for(int j = 0; j<gameEngine.getGridNumber()+3; j++){
                if(gameEngine.getPlayerAt(i,j) != GameEngine.Players.none){
                    Bitmap tmpBitmp = null;

                    if(gameEngine.getPlayerAt(i,j) == GameEngine.Players.playerX){
                        tmpBitmp = bitmapX;
                    }

                    if(gameEngine.getPlayerAt(i,j) == GameEngine.Players.playerO){
                        tmpBitmp = bitmapO;
                    }
                    canvas.drawBitmap(tmpBitmp, sizeOfElement*i, sizeOfElement*j, mPaint);
                }
            }
        }
    }

    private void LoadAndCreateBitmaps() {
        // Load bitmap
        bitmapX = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
        bitmapO = BitmapFactory.decodeResource(this.getResources(), R.drawable.o);


        // Resize bitmap
        bitmapX = Bitmap.createScaledBitmap(bitmapX, (int)sizeOfElement, (int)sizeOfElement, true);
        bitmapO = Bitmap.createScaledBitmap(bitmapO, (int)sizeOfElement, (int)sizeOfElement, true);

    }

    private void drawGrid(Canvas canvas) {

        for(int i = 0; i<gameEngine.getGridNumber() + 4; i++){
            // vẽ hàng ngang
            canvas.drawLine(0, sizeOfElement*i, gameEngine.getGridSize(), sizeOfElement*i, mPaint);


        }

        for(int i = 0; i<gameEngine.getGridNumber() +1; i++){
            // vẽ hàng dọc
            canvas.drawLine(sizeOfElement*i, 0, sizeOfElement*i, gameEngine.getGridSize()+500, mPaint);
        }

    }

    private int startX, startY, stopX, stopY;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        float sizeOfElement = gameEngine.getGridSize()*scaleFactor/gameEngine.getGridNumber();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                startX = (int)(event.getX()/sizeOfElement);
                startY = (int)(event.getY()/sizeOfElement);
                break;
            case MotionEvent.ACTION_UP:
                stopX = (int)(event.getX()/sizeOfElement);
                stopY = (int)(event.getY()/sizeOfElement);
                break;
            default:
                break;
        }

        if(startX < gameEngine.getGridNumber()+3 && startY <gameEngine.getGridNumber()+3){
            if(startX == stopX && startY == stopY){
                gameEngine.addMark(startX, startY);
            }
        }
        invalidate();
        return true;
    }


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();
            focusX = detector.getFocusX();
            focusY = detector.getFocusY();
            // don't let the object get too small or too large.
            scaleFactor = Math.max(1.0f, Math.min(scaleFactor, 3.0f));

            invalidate();
            return true;
        }
    }
}
