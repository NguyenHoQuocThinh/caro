package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/23/2018.
 */

public class ChangeIcon extends DialogFragment {
    private ArrayList<String> list = new ArrayList<>();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);



        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.change_icon, null))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RadioButton radioButtonX = (RadioButton)getView().findViewById(R.id.radioButtonX_ChangeIcon);
                        RadioButton radioButtonO = (RadioButton)getView().findViewById(R.id.radioButtonO_ChangeIcon);

                    }
                })
                .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        return builder.create();

    }
}
