package android.nguyenhoquocthinh.doancarop2p.Offline.HardMode;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nguyenhoquocthinh.doancarop2p.Offline.HardMode.HardModeGameEngine;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.view.View;

public class HardModeGraphics  extends View {
    private HardModeGameEngine hardModeGameEngine;
    private Paint mPaint;
    private Bitmap bitmapX, bitmapO;
    private float sizeOfElement;

    public HardModeGraphics (Context context, HardModeGameEngine hardModeGameEngine){
        super(context);

        this.hardModeGameEngine = hardModeGameEngine;
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(hardModeGameEngine.getGridRowSize() == 0){
            hardModeGameEngine.setGridRowSize(canvas.getHeight());
        }

        if(hardModeGameEngine.getGridColumnSize() == 0){
            hardModeGameEngine.setGridColumnSize(canvas.getWidth());

            sizeOfElement = hardModeGameEngine.getGridColumnSize() / hardModeGameEngine.getColumnNumber();
            loadAndCreateBitMap();
        }

        drawGrid(canvas);
        drawPlayers(canvas);

        invalidate();
    }

    private void drawPlayers(Canvas canvas) {
        for(int i = 0;i < hardModeGameEngine.getRowNumber();i ++){
            for(int j = 0; j < hardModeGameEngine.getColumnNumber();j ++){
                if(hardModeGameEngine.getPlayerAt(i, j) != HardModeGameEngine.Players.None){
                    Bitmap tempBitmap = null;
                    if (hardModeGameEngine.getPlayerAt(i, j) == HardModeGameEngine.Players.Players1) {
                        tempBitmap = bitmapX;
                    } else {
                        tempBitmap = bitmapO;
                    }
                    canvas.drawBitmap(tempBitmap, sizeOfElement * j, sizeOfElement * i, mPaint);
                }
            }
        }
    }

    private void loadAndCreateBitMap() {
        bitmapX = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
        bitmapO = BitmapFactory.decodeResource(this.getResources(), R.drawable.o);

        bitmapX = Bitmap.createScaledBitmap(bitmapX, (int)sizeOfElement, (int)sizeOfElement, true);
        bitmapO = Bitmap.createScaledBitmap(bitmapO, (int)sizeOfElement, (int)sizeOfElement, true);
    }

    private void drawGrid(Canvas canvas) {
        float rowSize = hardModeGameEngine.getGridRowSize();
        float columnSize = hardModeGameEngine.getGridColumnSize();

        for(int i = 0;i < hardModeGameEngine.getRowNumber() + 1;i ++){
            canvas.drawLine(0,columnSize / hardModeGameEngine.getColumnNumber() * i,
                    columnSize, columnSize/ hardModeGameEngine.getColumnNumber()* i, mPaint);
        }

        for(int i = 0;i < hardModeGameEngine.getColumnNumber() + 1;i ++){
            canvas.drawLine(columnSize / hardModeGameEngine.getColumnNumber() * i, 0,
                    columnSize / hardModeGameEngine.getColumnNumber() * i, rowSize, mPaint);
        }
    }
}
