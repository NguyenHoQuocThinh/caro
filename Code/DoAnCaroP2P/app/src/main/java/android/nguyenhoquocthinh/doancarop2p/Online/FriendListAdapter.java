package android.nguyenhoquocthinh.doancarop2p.Online;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nguyen Ho Quoc Thinh on 05/02/2018.
 */

public class FriendListAdapter extends ArrayAdapter<String> {

    private Context context;
    private String[] friendList;

    public FriendListAdapter(Context context, int resource, String[] friendList) {
        super(context, R.layout.list_friend_adapter, friendList);
        this.context = context;
        this.friendList = friendList;
    }

    private class ViewHolder{
        TextView textViewUsername;
        ImageView imageViewState;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            convertView = vi.inflate(R.layout.list_friend_adapter, null);
            holder = new ViewHolder();
            holder.textViewUsername = (TextView)convertView.findViewById(R.id.textViewUsername_ListFriend);
            holder.imageViewState = (ImageView) convertView.findViewById(R.id.buttonState);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        String tmp = friendList[position];
        String data[] = tmp.split(":");
        holder.textViewUsername.setText(data[0]);
        if(data[1].equals("online")){
            holder.imageViewState.setBackgroundResource(R.drawable.online);
        }else if(data[1].equals("busy")){
            holder.imageViewState.setBackgroundResource(R.drawable.busy);
        }else if(data[1].equals("offline")){
            holder.imageViewState.setBackgroundResource(R.drawable.offline);
        }
        return convertView;
    }
}
