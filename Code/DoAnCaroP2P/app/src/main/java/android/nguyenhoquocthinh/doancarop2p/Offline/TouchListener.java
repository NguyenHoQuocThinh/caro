package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.nguyenhoquocthinh.doancarop2p.Offline.GameEngine;
import android.view.MotionEvent;
import android.view.View;

public class TouchListener implements View.OnTouchListener {

    private GameEngine gameEngine;

    private int startX, startY, stopX, stopY;

    public TouchListener(GameEngine gameEngine){
        this.gameEngine = gameEngine;
    }

    public boolean onTouch(View arg0, MotionEvent event){
        float sizeOfElement = gameEngine.getGridColumnSize() / gameEngine.getColumnNumber();

        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                startX = (int)(event.getX() / sizeOfElement);
                startY = (int) (event.getY() / sizeOfElement);
                break;
            case MotionEvent.ACTION_UP:
                stopX = (int)(event.getX() / sizeOfElement);
                stopY = (int) (event.getY() / sizeOfElement);
                break;
        }

        if(startY < gameEngine.getRowNumber() && startX < gameEngine.getColumnNumber()){
            if(startX == stopX && startY == stopY){
                gameEngine.addMark(startY, startX);
                gameEngine.machineMoves();
            }
        }

        return true;
    }
}
