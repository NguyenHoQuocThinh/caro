package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.UnicodeSetSpanner;
import android.nguyenhoquocthinh.doancarop2p.PlayWithFriend.GameEngine;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class WifiP2pASyncTask extends AsyncTask<Integer, Integer, Void> {
    private Activity mActivity;
    private android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine gameEngine;

    private android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine.Players player1;
    private FrameLayout frameLayout;
    private Button buttonExit;
    private ImageButton imageButton1;
    ServerSocket serverSocket = null;
    Socket enemy;

    private boolean clientCome = false;

    public WifiP2pASyncTask(Activity mActivity){
        this.mActivity = mActivity;
    }

    private void Initialize() {
        buttonExit = (Button)mActivity.findViewById(R.id.buttonExit_P2P);
        imageButton1 = (ImageButton)mActivity.findViewById(R.id.imageButtonP2P);
    }

    @Override
    protected Void doInBackground(final Integer... integers) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(enemy == null){
                        enemy = serverSocket.accept();
                    }

                    if(enemy != null && integers[0] != -1 && integers[1] != -1){
                        OutputStream outputStream = enemy.getOutputStream();
                        byte[] b = new byte[2];
                        b[0] = integers[0].byteValue();
                        b[1] = integers[1].byteValue();

                        outputStream.write(b);
                        outputStream.flush();

                        clientCome = false;
                        enemy = null;
                        enemy = serverSocket.accept();
                    }

                    if (enemy != null) {
                        InputStream inputStream = enemy.getInputStream();

                        if (inputStream != null) {

                            int pos[] = new int[2];
                            int num = -1;

                            pos[0] = inputStream.read();
                            pos[1] = inputStream.read();


                            if(pos[0] != -1 && pos[1] != -1) {
                                clientCome = true;
                                publishProgress(pos[0], pos[1]);
                            }
                        }
                    }
                } catch (IOException e) {
                    Log.e("WIFI P2P ASyncTask: ", e.getMessage());
                    e.printStackTrace();
                }
            }
        }).start();
        return null;
    }

    @Override
    protected void onPreExecute(){
        if(gameEngine == null) {
            Initialize();

            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//
//                    try {
//                        OutputStream out= enemy.getOutputStream();
//                        byte[] b = new byte[2];
//                        int a[]={-100,0};
//                        b[0]=(byte)a[0];
//                        b[1]=(byte)a[1];
//                        out.write(b);
//
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }

                    mActivity.finish();
                }
            });

            player1 = android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine.Players.Players1;
            imageButton1.setImageResource(R.drawable.x);

            gameEngine = new android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine(mActivity, player1, imageButton1);
            frameLayout = (FrameLayout) mActivity.findViewById(R.id.p2pFrame);
            frameLayout.setOnTouchListener(new TouchListener(gameEngine, this));
            frameLayout.addView(new Graphics(mActivity, gameEngine));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        serverSocket = new ServerSocket(3333);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    @Override
        protected void onProgressUpdate(Integer...values){
        if(gameEngine.getCurrentPlayer() == gameEngine.getP())
            gameEngine.SwitchPlayer();

        gameEngine.addMark(values[0], values[1]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
    }

    public boolean isClientCome() {
        return clientCome;
    }
}
