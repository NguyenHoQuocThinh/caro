package android.nguyenhoquocthinh.doancarop2p.Online;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Nguyen Ho Quoc Thinh on 05/01/2018.
 */

public class HistoryAdapter extends ArrayAdapter<String> {

    private Context context;
    private String[] playerIcons;
    private String[] results;
    private String[] opponents;

    public HistoryAdapter(Context context, int layoutToBeInflated, String[] playerIcons, String[] opponents,String[] results) {
        super(context, R.layout.custom_history_adapter, results);
        this.context = context;
        this.playerIcons = playerIcons;
        this.results = results;
        this.opponents = opponents;
    }

    private class ViewHolder{
        TextView textViewResult;
        TextView textViewVS;
        ImageView imageViewPlayer;
        ImageView imageViewOpponent;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            convertView = vi.inflate(R.layout.custom_history_adapter, null);
            holder = new ViewHolder();
            holder.textViewResult = (TextView)convertView.findViewById(R.id.textView15);
            holder.textViewVS = (TextView)convertView.findViewById(R.id.textView16);
            holder.imageViewPlayer = (ImageView)convertView.findViewById(R.id.imageViewPlayer);
            holder.imageViewOpponent = (ImageView)convertView.findViewById(R.id.imageViewOpponent);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        holder.textViewVS.setText(opponents[position]);

        if(playerIcons[position].compareTo("X")==0){
            holder.imageViewPlayer.setImageResource(R.drawable.x);
            holder.imageViewOpponent.setImageResource(R.drawable.o);
        }
        else
        {
            holder.imageViewPlayer.setImageResource(R.drawable.o);
            holder.imageViewOpponent.setImageResource(R.drawable.x);
        }

        if(results[position].compareTo("W")==0){
            holder.textViewResult.setText("THẮNG");
            holder.textViewResult.setTextColor(Color.GREEN);
        }else if(results[position].compareTo("L")==0) {
            holder.textViewResult.setText("THUA");
            holder.textViewResult.setTextColor(Color.RED);
        }else{
            holder.textViewResult.setText("HÒA");
            holder.textViewResult.setTextColor(Color.GRAY);
        }



        return convertView;
    }
}
