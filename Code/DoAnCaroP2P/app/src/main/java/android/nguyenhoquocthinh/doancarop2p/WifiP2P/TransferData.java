package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TransferData extends AsyncTask<Integer, Integer, Void>{
    private Socket socket;
    private int port;
    private String host;

    private Activity mActivity;
    private android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine gameEngine;
    private android.nguyenhoquocthinh.doancarop2p.WifiP2P.GameEngine.Players player1;
    private FrameLayout frameLayout;
    private Button buttonExit;
    private ImageButton imageButton1;

    public Socket getSocket(){ return this.socket;}

    public TransferData(Activity mActivity, int port, String host){
        this.mActivity = mActivity;
        this.port = port;
        this.host = host;

        socket = new Socket();
    }


    private void Initialize() {
        buttonExit = (Button)mActivity.findViewById(R.id.buttonExit_P2P);
        imageButton1 = (ImageButton)mActivity.findViewById(R.id.imageButtonP2P);
    }

    public void transferData(final int[] pos){
        final byte[] b = new byte[2];
        b[0] = (byte)pos[0];
        b[1] = (byte)pos[1];

        try{
            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(b);
            outputStream.flush();

            InputStream inputStream = socket.getInputStream();

            byte[] ba = new byte[2];
            ba[0] = -1;
            ba[1] = -1;

                while (ba[0] == -1 && ba[1] == -1) {
                    inputStream.read(ba);
                    publishProgress((int) ba[0], (int) ba[1]);
                }

                if (socket != null) {
                    socket = null;
                    socket = new Socket();
                }

        } catch(IOException e){
            Log.e("TRANSFERDATA: ", e.getMessage());
            Log.e("STACK TRACE: ", "\n");
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(final Integer... integers) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(!socket.isConnected()) {
                    try {
                        socket.bind(null);
                        InetSocketAddress i = new InetSocketAddress(host, port);
                        if (i != null)
                            socket.connect(i, 50000);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                int pos[] = new int[2];
                pos[0] = integers[0];
                pos[1] = integers[1];
                if (pos[0] != -1 && pos[1] != -1)
                    transferData(pos);
            }
        }).start();

        return null;
    }

    @Override
    protected void onPreExecute(){
        if(gameEngine == null) {
            Initialize();

            player1 = GameEngine.Players.Players2;
            imageButton1.setImageResource(R.drawable.o);

            gameEngine = new GameEngine(mActivity, player1, imageButton1);
            frameLayout = (FrameLayout) mActivity.findViewById(R.id.p2pFrame);
            frameLayout.setOnTouchListener(new TouchListener(gameEngine, this));
            frameLayout.addView(new Graphics(mActivity, gameEngine));

            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//
//                    try {
//                        byte[] b = new byte[2];
//                        int a[]={-100,0};
//                        b[0]=(byte)a[0];
//                        b[1]=(byte)a[1];
//                        OutputStream out = socket.getOutputStream();
//                        out.write(b);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }


                    mActivity.finish();
                }
            });
        }
    }

    @Override
    protected void onProgressUpdate(Integer...values){
        gameEngine.addMark(values[0], values[1]);
    }

    @Override
    protected void onPostExecute(Void aVoid){

    }
}
