package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.MainActivity;

public class EndDialog extends AlertDialog {
    private Context context;
    private GameEngine gameEngine;


    protected EndDialog(final Context context, String winner, final GameEngine gameEngine) {
        super(context);
        this.context = context;
        this.gameEngine = gameEngine;

        String mes = winner;


        setMessage(mes);

        setButton(BUTTON_POSITIVE, "Làm thêm ván nữa", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Exit();
                Intent intent = new Intent(EndDialog.this.context, BeforePlayingP2P.class);
                intent.putExtra("disconnect","true");
                EndDialog.this.context.startActivity(intent);

            }
        });

        setButton(BUTTON_NEGATIVE, "Thôi nghỉ chơi", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Exit();
//                Intent intent = new Intent(EndDialog.this.context, MainActivity.class);
//                EndDialog.this.context.startActivity(intent);
            }
        });

        show();

        this.setCancelable(false);

    }

    private void Exit() {
        Activity a = (Activity)context;
        a.finish();
    }
}
