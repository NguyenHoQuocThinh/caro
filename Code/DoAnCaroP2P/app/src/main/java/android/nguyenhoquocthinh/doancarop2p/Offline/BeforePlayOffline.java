package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.Offline.HardMode.PlayOfflineHardMode;
import android.nguyenhoquocthinh.doancarop2p.Online.BeforePlayOnline;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class BeforePlayOffline extends AppCompatActivity {
    private Button btnPlayOfflineMain;
    private RadioButton rbtnEasy;
    private RadioButton rbtnHard;
    //private RadioButton rbtnEasy;
    private RadioButton rbtnX;
    private RadioButton rbtnO;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.before_play_with_machine);

        btnPlayOfflineMain = (Button)findViewById(R.id.btnPlay_PlayOffline);
        rbtnEasy = (RadioButton)findViewById(R.id.radioButtonEasy);
        rbtnHard = (RadioButton)findViewById(R.id.radioButtonHard);
        rbtnX = (RadioButton)findViewById(R.id.radioButtonX);
        rbtnO = (RadioButton)findViewById(R.id.radioButtonO);

        btnPlayOfflineMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rbtnEasy.isChecked() || rbtnHard.isChecked()) {
                    if (rbtnX.isChecked() || rbtnO.isChecked()) {
                        if (rbtnEasy.isChecked()) {
                            String icon = "";
                            if (rbtnX.isChecked())
                                icon = "X";
                            else if (rbtnO.isChecked())
                                icon = "0";
                            finish();
                            Intent intent = new Intent(BeforePlayOffline.this, PlayOffline.class);
                            intent.putExtra("Icon", icon);
                            startActivity(intent);
                        } else if (rbtnHard.isChecked()) {
                            String icon = "";
                            if (rbtnX.isChecked())
                                icon = "X";
                            else if (rbtnO.isChecked())
                                icon = "0";
                            finish();
                            Intent intent = new Intent(BeforePlayOffline.this, PlayOfflineHardMode.class);
                            intent.putExtra("Icon", icon);
                            startActivity(intent);
                        }
                    }
                    else
                        Toast.makeText(BeforePlayOffline.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(BeforePlayOffline.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
