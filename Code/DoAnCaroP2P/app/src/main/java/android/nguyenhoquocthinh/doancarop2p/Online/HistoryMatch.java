package android.nguyenhoquocthinh.doancarop2p.Online;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/30/2018.
 */

public class HistoryMatch extends AppCompatActivity{

    private Button buttonExit;
    private ListView listViewOnlineHistory;
    private Socket socket;
    private Player player;
    private Dialog dialog;
    private String[] playerIcons;
    private String[] opponent;
    private String[] results;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_history);

        Intent intent = getIntent();
        player = (Player) intent.getSerializableExtra("player");

        socket = SocketHandler.getSocket();


        socket.on("HistoryListFeedback", HistoryListFeedback);
        socket.on("OpponentInfoFeedback", OpponentInfoFeedback);
        socket.on("FindRandomPlayerFeedback", FindRandomPlayerFeedback);
        socket.on("ChallengeAFriendFeedback", ChallengeAFriendFeedback);
        Initialize();

        ShowHistoryMatch();

        listViewOnlineHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ShowOpponentInfo(i);
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void ShowOpponentInfo(int i) {
        String data = opponent[i].substring(opponent[i].lastIndexOf(" ")+1);
        socket.emit("OpponentInfo", data);
    }


    private void ShowHistoryMatch() {
        socket.emit("HistoryList", player.getUsername());
    }



    private void Initialize() {
        buttonExit = (Button)findViewById(R.id.buttonExit_OnlineHistory);
        listViewOnlineHistory = (ListView)findViewById(R.id.listViewOnlineHistory);
    }

    private Emitter.Listener HistoryListFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        if(mess.length()!=0){
                            String data[]  = mess.split("-");
                            opponent = new String[data.length];
                            playerIcons = new String[data.length];
                            results = new String[data.length];
                            SetArrayList(data);
                            HistoryAdapter adapter = new HistoryAdapter(HistoryMatch.this,
                                    R.layout.custom_history_adapter,
                                    playerIcons,
                                    opponent,
                                    results);
                            listViewOnlineHistory.setAdapter(adapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void SetArrayList(String[] data) {
        int k = 0;
        if(data.length!=0){
            for(int i = data.length-1; i>=0; i--){
                String[] items = data[i].split(":");
                String tmp = player.getUsername()+" vs "+items[0];
                opponent[k] = tmp;
                playerIcons[k] = items[1];
                results[k] = items[2];
                k++;
            }
        }
    }

    private Emitter.Listener OpponentInfoFeedback= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        OpponentInfoDialog(mess);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void OpponentInfoDialog(String mess) {
        if(!((Activity)this).isFinishing()){
            String data[] = mess.split("-");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View v = getLayoutInflater().inflate(R.layout.opponent_info, null);
            final TextView textViewUsername = (TextView)v.findViewById(R.id.textViewUsername_OpponentInfo);
            Button rank = (Button)v.findViewById(R.id.buttonRank);
            TextView textViewWin = (TextView)v.findViewById(R.id.textViewWin_OpponentInfo);
            TextView textViewLose = (TextView)v.findViewById(R.id.textViewLose_OpponentInfo);
            Button buttonAddFriend = (Button)v.findViewById(R.id.buttonAddFriend);
            rank.setEnabled(false);
            textViewUsername.setText(data[0]);
            textViewWin.setText("Số trận thắng: "+data[1]);
            textViewLose.setText("Số trận thua: "+data[2]);
            switch (data[3]){
                case "1":
                    // đồng
                    rank.setBackgroundResource(R.drawable.bronze);
                    break;
                case "2":
                    // bạc
                    rank.setBackgroundResource(R.drawable.silver);
                    break;
                case "3":
                    // vàng
                    rank.setBackgroundResource(R.drawable.gold);
                    break;
                case "4":
                    // bạch kim
                    rank.setBackgroundResource(R.drawable.plat);
                    break;
                case "5":
                    // kim cương
                    rank.setBackgroundResource(R.drawable.diamond);
                    break;
            }


            builder.setView(v);
            dialog = builder.create();
            dialog.show();

            buttonAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String x = player.getUsername() + "-" + textViewUsername.getText().toString();
                    //Toast.makeText(HistoryMatch.this, "Đã thêm " + x + " vào danh sách bạn", Toast.LENGTH_SHORT).show();
                    socket.emit("AddFriend", x);
                    dialog.dismiss();
                }
            });
        }
    }

    private Emitter.Listener FindRandomPlayerFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HistoryMatch.this.finish();
                }
            });
        }
    };

    private Emitter.Listener ChallengeAFriendFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HistoryMatch.this.finish();
                }
            });
        }
    };

    public void onBackPressed() {
        //super.onBackPressed();
    }

}
