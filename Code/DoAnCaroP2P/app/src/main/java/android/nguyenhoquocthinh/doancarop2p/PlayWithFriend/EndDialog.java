package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.MainActivity;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/23/2018.
 */

public class EndDialog extends AlertDialog{

    private  Context context;
    private GameEngine gameEngine;
    private String message;

    protected EndDialog(Context context, String winner, final GameEngine.Players player, final GameEngine gameEngine) {
        super(context);
        this.context = context;
        this.gameEngine = gameEngine;

        setTitle("Kết quả");

        String mes;
        if(winner.equals("none"))
            mes = "Hòa";
        else
            mes = "Người thắng cuộc là: " + winner;
        setMessage(mes);
        setButton(BUTTON_POSITIVE, "Chơi lại", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                gameEngine.StartGame(player);
            }
        });

        setButton(BUTTON_NEGATIVE, "Nghỉ", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Exit();
            }
        });

        show();

        this.setCancelable(false);

    }

    private void Exit() {
        Activity a = (Activity)context;
        a.finish();
    }
}
