package android.nguyenhoquocthinh.doancarop2p.Online;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import android.nguyenhoquocthinh.doancarop2p.Online.*;
/**
 * Created by Nguyen Ho Quoc Thinh on 05/02/2018.
 */

public class FriendList extends AppCompatActivity {

    private ListView listViewFriends;
    private Button buttonExit;
    private Socket socket;
    private Player player;
    private String [] friendList;
    private Dialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_friend_list);

        Intent intent = getIntent();
        player = (Player) intent.getSerializableExtra("player");

        socket = SocketHandler.getSocket();

        socket.on("GetFriendsFeedback", GetFriendsFeedback);
        socket.on("OpponentInfoFeedback", OpponentInfoFeedback);
        socket.on("ChallengeAFriendFeedback", ChallengeAFriendFeedback);


        Initialize();
        LoadList();

        listViewFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String tmp = friendList[i];
                String state[] = tmp.split(":");
                if(state[1].equals("online"))
                    Challenge(i);
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(FriendList.this);
                    builder.setMessage("Người chơi hiện đang offline hoặc không thể nhận lời thách đấu. Vui lòng thử lại sau");
                    final AlertDialog dialog2 = builder.create();
                    dialog2.setCancelable(false);
                    dialog2.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialog2.dismiss();
                        }
                    });
                    dialog2.show();
                }
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void Challenge(int i) {
        //Toast.makeText(this, "fdfs", Toast.LENGTH_SHORT).show();
        String data = friendList[i].substring(0, friendList[i].indexOf(":"));
        socket.emit("OpponentInfo", data);
    }

    private void LoadList() {
        socket.emit("GetFriends", player.getUsername());
    }

    private void Initialize() {
        listViewFriends = (ListView)findViewById(R.id.listViewFriends);
        buttonExit = (Button)findViewById(R.id.buttonExit_FriendList);
    }

    private Emitter.Listener GetFriendsFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        if(mess.equals("")==false){
                            String data[] = mess.split("-");
                            friendList = new String[data.length];
                            SetFriendList(data);
                            FriendListAdapter adapter = new FriendListAdapter(FriendList.this,
                                    R.layout.list_friend_adapter,
                                    friendList);
                            listViewFriends.setAdapter(adapter);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void SetFriendList(String[] data) {
        int k = 0;
        if(data.length!=0){
            for(int i = 0; i<friendList.length; i++){
                friendList[k] = data[i];
                k++;
            }
        }
    }

    private Emitter.Listener OpponentInfoFeedback= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        ChallengeDialog(mess);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void ChallengeDialog(String mess) {
        if(!((Activity)this).isFinishing()){
            String data[] = mess.split("-");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View v = getLayoutInflater().inflate(R.layout.friend_challenge_dialog, null);
            final TextView textViewUsername = (TextView)v.findViewById(R.id.textViewUsername_OpponentInfo);
            Button rank = (Button)v.findViewById(R.id.buttonRank);
            TextView textViewWin = (TextView)v.findViewById(R.id.textViewWin_OpponentInfo);
            TextView textViewLose = (TextView)v.findViewById(R.id.textViewLose_OpponentInfo);
            Button buttonChallenge = (Button)v.findViewById(R.id.buttonChallenge);
            rank.setEnabled(false);
            textViewUsername.setText(data[0]);
            textViewWin.setText("Số trận thắng: "+data[1]);
            textViewLose.setText("Số trận thua: "+data[2]);
            switch (data[3]){
                case "1":
                    // đồng
                    rank.setBackgroundResource(R.drawable.bronze);
                    break;
                case "2":
                    // bạc
                    rank.setBackgroundResource(R.drawable.silver);
                    break;
                case "3":
                    // vàng
                    rank.setBackgroundResource(R.drawable.gold);
                    break;
                case "4":
                    // bạch kim
                    rank.setBackgroundResource(R.drawable.plat);
                    break;
                case "5":
                    // kim cương
                    rank.setBackgroundResource(R.drawable.diamond);
                    break;
            }


            builder.setView(v);
            dialog = builder.create();
            dialog.show();

            buttonChallenge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String x = player.getUsername() + "-" + textViewUsername.getText().toString();
                    socket.emit("ChallengeAFriend", x);
                    dialog.dismiss();
                }
            });
        }
    }

    
    private Emitter.Listener ChallengeAFriendFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    FriendList.this.finish();
                }
            });
        }
    };

    public void onBackPressed() {
        //super.onBackPressed();
    }

}
