package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.app.Activity;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/22/2018.
 */

public class BeforePlayingWithFriend extends AppCompatActivity {

    private RadioGroup radioGroupPlayer1Icon;
    private RadioGroup radioGroupPlayer2Icon;
    private RadioButton radioButtonX1;
    private RadioButton radioButtonX2;
    private RadioButton radioButtonO1;
    private RadioButton radioButtonO2;
    private Button buttonPlay;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_with_friend_before_game);

        Initialize();

        radioGroupPlayer1Icon.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioButtonX1.isChecked()){
                    radioButtonO2.setChecked(true);
                }else{
                    radioButtonX2.setChecked(true);
                }
            }
        });

        radioGroupPlayer2Icon.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioButtonX2.isChecked()){
                    radioButtonO1.setChecked(true);
                }else{
                    radioButtonX1.setChecked(true);
                }
            }
        });


        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!radioButtonO1.isChecked() && !radioButtonO2.isChecked() && !radioButtonX1.isChecked() && !radioButtonX2.isChecked()){
                    Toast.makeText(BeforePlayingWithFriend.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(BeforePlayingWithFriend.this, Playing_PlayWithFriend.class);
                    String data = "";
                    if(radioButtonX1.isChecked()){
                        data = "X";
                    }
                    else if(radioButtonO1.isChecked()){
                        data = "O";
                    }
                    intent.putExtra("PlayersIcon", data);
                    startActivity(intent);
                    Activity a = (Activity)BeforePlayingWithFriend.this;
                    a.finish();
                }
            }
        });
    }

    private void Initialize() {
        radioGroupPlayer1Icon = (RadioGroup)findViewById(R.id.radioGroupPlayer1Icon);
        radioGroupPlayer2Icon = (RadioGroup)findViewById(R.id.radioGroupPlayer2Icon);
        radioButtonO1 = (RadioButton) findViewById(R.id.radioButtonO1_PlayWithFriend);
        radioButtonO2 = (RadioButton)findViewById(R.id.radioButtonO2_PlayWithFriend);
        radioButtonX1 = (RadioButton) findViewById(R.id.radioButtonX1_PlayWithFriend);
        radioButtonX2 = (RadioButton)findViewById(R.id.radioButtonX2_PlayWithFriend);
        buttonPlay = (Button) findViewById(R.id.buttonPlay_PlayWithFriend);

    }
}
