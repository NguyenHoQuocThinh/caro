package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.MainActivity;
import android.nguyenhoquocthinh.doancarop2p.Offline.HardMode.HardModeGameEngine;
import android.nguyenhoquocthinh.doancarop2p.Offline.HardMode.PlayOfflineHardMode;
import android.nguyenhoquocthinh.doancarop2p.Online.Player;
import android.view.View;

public class EndDialog extends AlertDialog {
    private Context context;
    private GameEngine gameEngine;
    private HardModeGameEngine hardModeGameEngine;
    private String message;
    protected EndDialog(Context context, String winner, final GameEngine.Players player, final GameEngine gameEngine) {
        super(context);
        this.context = context;
        this.gameEngine = gameEngine;

        String mes = winner;

        setMessage(mes);
        setButton(BUTTON_POSITIVE, "Làm thêm ván nữa", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                gameEngine.StartGame(player);
            }
        });

        setButton(BUTTON_NEGATIVE, "Thôi nghỉ chơi", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Exit();
                Intent intent = new Intent(EndDialog.this.context, MainActivity.class);
                EndDialog.this.context.startActivity(intent);
            }
        });

        show();

        this.setCancelable(false);

    }

    public EndDialog(Context context, String winner, final HardModeGameEngine.Players player, final HardModeGameEngine hardModeGameEngine) {
        super(context);
        this.context = context;
        this.hardModeGameEngine = hardModeGameEngine;

        String mes = winner;

        setMessage(mes);
        setButton(BUTTON_POSITIVE, "Làm thêm ván nữa", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                hardModeGameEngine.StartGame(player);
            }
        });

        setButton(BUTTON_NEGATIVE, "Thôi nghỉ chơi", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Exit();
                Intent intent = new Intent(EndDialog.this.context, MainActivity.class);
                EndDialog.this.context.startActivity(intent);
            }
        });

        show();

        this.setCancelable(false);

    }

    private void Exit() {
        Activity a = (Activity)context;
        a.finish();
    }
}
