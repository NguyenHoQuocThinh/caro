package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.MainActivity;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

public class PlayOffline extends AppCompatActivity {
    private GameEngine gameEngine;
    private FrameLayout gameFrame;

    private Button btnAgain;
    private Button btnExit;
    private Button btnRefresh;

    private ImageButton ibtn1;
    private ImageButton ibtn2;

    private GameEngine.Players p = GameEngine.Players.None;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_with_machine_main);

        Intent intent = getIntent();
        String icon = intent.getStringExtra("Icon");

        initialize();

        if(icon.equals("X")){
            p = GameEngine.Players.Players1;
            ibtn1.setImageResource(R.drawable.x);
            ibtn2.setImageResource(R.drawable.o);
        }
        else{
            p = GameEngine.Players.Players2;
            ibtn1.setImageResource(R.drawable.o);
            ibtn2.setImageResource(R.drawable.x);
        }

        MachineEngine machineEngine = new MachineEngine();
        gameEngine = new GameEngine(machineEngine, this, p, ibtn1, ibtn2);
        gameFrame = (FrameLayout)findViewById(R.id.gameFrame);
        gameFrame.setOnTouchListener(new TouchListener(gameEngine));
        gameFrame.addView(new Graphics(this, gameEngine));

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameEngine.StartGame(p);
            }
        });

        btnAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameEngine.MoveAgain();
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(PlayOffline.this, MainActivity.class);

                startActivity(intent);
            }
        });

        ibtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ChangeIconDialog(1);

            }
        });

        ibtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ChangeIconDialog(2);
            }
        });
    }

    private void initialize(){
        btnAgain = (Button)findViewById(R.id.btnAgain_PlayOffline);
        btnExit = (Button)findViewById(R.id.btnExit_PlayOffline);
        btnRefresh = (Button)findViewById(R.id.btnNewGame_PlayOffline);

        ibtn1 = (ImageButton)findViewById(R.id.imageButton1);
        ibtn2 = (ImageButton)findViewById(R.id.imageButton2);
    }
}
