package android.nguyenhoquocthinh.doancarop2p;

import android.app.Dialog;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.Offline.BeforePlayOffline;
import android.nguyenhoquocthinh.doancarop2p.Offline.HardMode.PlayOfflineHardMode;
import android.nguyenhoquocthinh.doancarop2p.Offline.PlayOffline;
import android.nguyenhoquocthinh.doancarop2p.Online.BeforePlayOnline;
import android.nguyenhoquocthinh.doancarop2p.PlayWithFriend.BeforePlayingWithFriend;
import android.nguyenhoquocthinh.doancarop2p.PlayWithFriend.Playing_PlayWithFriend;
import android.nguyenhoquocthinh.doancarop2p.WifiP2P.BeforePlayingP2P;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonPlayOnline;
    private Button buttonPlayWithFriend;
    private Button buttonPlayOffline;
    private Button buttonWifiP2P;
    private Button buttonGameInfo;
    private Button buttonExit;
    private Dialog PlayWithFriendDialog;
    private Dialog PlayWithMachineDialog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Initialize(); // ánh xạ các thành phần

        buttonPlayOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayWithMachineDialog();
//                Intent intent = new Intent(MainActivity.this, BeforePlayOffline.class);
//                MainActivity.this.startActivity(intent);
            }
        });

        buttonPlayWithFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayWithFriendDialog();
            }
        });

        buttonPlayOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BeforePlayOnline.class);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonWifiP2P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BeforePlayingP2P.class);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonGameInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GameInfo.class);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void PlayWithMachineDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = getLayoutInflater().inflate(R.layout.play_with_machine_dialog, null);
        final RadioButton radioButton_X = (RadioButton)v.findViewById(R.id.radioButton_X);
        final RadioButton radioButton_O = (RadioButton)v.findViewById(R.id.radioButton_O);
        final RadioButton radioButton_Easy = (RadioButton)v.findViewById(R.id.radioButton_Easy);
        final RadioButton radioButton_Hard = (RadioButton)v.findViewById(R.id.radioButton_Hard);
        Button button_Play = (Button)v.findViewById(R.id.button_Play);

        button_Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(radioButton_Easy.isChecked()==false && radioButton_Hard.isChecked()==false){
                    Toast.makeText(MainActivity.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                }else{
                    if(radioButton_Easy.isChecked()==true){
                        if(radioButton_X.isChecked()==false && radioButton_O.isChecked()==false)
                            Toast.makeText(MainActivity.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                        else{
                            Intent intent = new Intent(MainActivity.this, PlayOffline.class);
                            String message = "";
                            if(radioButton_X.isChecked()==true)
                                message = "X";
                            if(radioButton_O.isChecked()==true)
                                message = "O";
                            intent.putExtra("Icon", message);
                            startActivity(intent);
                        }
                    }
                    if(radioButton_Hard.isChecked()==true){
                        if(radioButton_X.isChecked()==false && radioButton_O.isChecked()==false)
                            Toast.makeText(MainActivity.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                        else{
                            Intent intent = new Intent(MainActivity.this, PlayOfflineHardMode.class);
                            PlayWithMachineDialog.dismiss();
                            String message = "";
                            if(radioButton_X.isChecked()==true)
                                message = "X";
                            if(radioButton_O.isChecked()==true)
                                message = "O";
                            intent.putExtra("Icon", message);
                            startActivity(intent);
                        }
                    }

                }



            }
        });
        builder.setView(v);
        PlayWithMachineDialog = builder.create();
        PlayWithMachineDialog.show();
    }

    private void PlayWithFriendDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = getLayoutInflater().inflate(R.layout.play_with_friend_dialog, null);
        final RadioButton radioButton_X = (RadioButton)v.findViewById(R.id.radioButton_X);
        final RadioButton radioButton_O = (RadioButton)v.findViewById(R.id.radioButton_O);
        Button button_Play = (Button)v.findViewById(R.id.button_Play);

        button_Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(radioButton_X.isChecked()==false && radioButton_O.isChecked()==false)
                    Toast.makeText(MainActivity.this, "Chưa thể bắt đầu trò chơi", Toast.LENGTH_SHORT).show();
                else{
                    Intent intent = new Intent(MainActivity.this, Playing_PlayWithFriend.class);
                    String message = "";
                    if(radioButton_X.isChecked()==true)
                        message = "X";
                    if(radioButton_O.isChecked()==true)
                        message = "O";
                    intent.putExtra("PlayersIcon", message);
                    startActivity(intent);
                    PlayWithFriendDialog.dismiss();
                }
            }
        });
        builder.setView(v);
        PlayWithFriendDialog = builder.create();
        PlayWithFriendDialog.show();
    }

    private void Initialize() {
        buttonPlayOnline = (Button)findViewById(R.id.buttonPlayOnline);
        buttonPlayWithFriend = (Button)findViewById(R.id.buttonPlayWithFriend);
        buttonPlayOffline = (Button)findViewById(R.id.buttonPlayOffline);
        buttonWifiP2P = (Button)findViewById(R.id.buttonWifiP2p);
        buttonGameInfo = (Button)findViewById(R.id.buttonGameInfo);
        buttonExit = (Button)findViewById(R.id.buttonExit);
    }




}
