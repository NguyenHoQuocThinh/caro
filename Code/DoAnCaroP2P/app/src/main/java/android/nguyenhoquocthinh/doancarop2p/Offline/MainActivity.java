package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    private Button btnPlayWithOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPlayWithOffline = (Button)findViewById(R.id.buttonPlayOffline);

        btnPlayWithOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent beforePlayOffline = new Intent(MainActivity.this, BeforePlayOffline.class);

                startActivity(beforePlayOffline);
            }
        });
    }

}
