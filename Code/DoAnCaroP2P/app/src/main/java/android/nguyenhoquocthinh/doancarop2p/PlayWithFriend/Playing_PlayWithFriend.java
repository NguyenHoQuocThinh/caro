package android.nguyenhoquocthinh.doancarop2p.PlayWithFriend;

import android.content.Intent;
import android.graphics.Color;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/22/2018.
 */

public class Playing_PlayWithFriend extends AppCompatActivity{

    private GameEngine gameEngine;
    private GameEngine.Players player1;
    private FrameLayout frameLayout;
    private Button buttonNewGame;
    private Button buttonAgain;
    private Button buttonExit;
    private TextView textViewPlayer1;
    private TextView textViewPlayer2;
    private ImageButton imageButton1;
    private ImageButton imageButton2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_with_friend_main);
//        View decorView = getWindow().getDecorView();
//        int uiOption = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOption);
        Initialize();

        Intent intent = getIntent();
        String data = "";
        data = intent.getStringExtra("PlayersIcon");
        if(data.compareTo("X")==0){
            player1 = GameEngine.Players.playerX;
            textViewPlayer1.setText("X");
            textViewPlayer1.setTextColor(Color.RED);
            imageButton1.setImageResource(R.drawable.x);
            textViewPlayer2.setText("O");
            textViewPlayer2.setTextColor(Color.GREEN);
            imageButton2.setImageResource(R.drawable.o);
        }
        else{
            player1 = GameEngine.Players.playerO;
            textViewPlayer1.setText("O");
            textViewPlayer1.setTextColor(Color.GREEN);
            imageButton1.setImageResource(R.drawable.o);
            textViewPlayer2.setText("X");
            textViewPlayer2.setTextColor(Color.RED);
            imageButton2.setImageResource(R.drawable.x);
        }
        gameEngine = new GameEngine(this, player1, imageButton1, imageButton2);
        frameLayout = (FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.addView(new Graphic(this, gameEngine));

        buttonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameEngine.StartGame(player1);
            }
        });

        buttonAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameEngine.MoveAgain();
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ChangeIconDialog(1);

            }
        });
        
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ChangeIconDialog(2);
            }
        });
        
    }

//    private void ChangeIconDialog(int index) {
//        if(index == 1){
//            final AlertDialog.Builder builder = new AlertDialog.Builder(Playing_PlayWithFriend.this);
//            View v = getLayoutInflater().inflate(R.layout.change_icon, null);
//            final RadioButton radioButtonX = (RadioButton)v.findViewById(R.id.radioButtonX_ChangeIcon);
//            final RadioButton radioButtonY = (RadioButton)v.findViewById(R.id.radioButtonO_ChangeIcon);
//            final Button buttonOK = (Button)v.findViewById(R.id.buttonOK_ChangeIcon);
//            final Button buttonCancel = (Button)v.findViewById(R.id.buttonCancel_ChangeIcon);
//            builder.setView(v);
//            final AlertDialog dialog = builder.create();
//            dialog.show();
//            buttonOK.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(radioButtonX.isChecked()){
//                        if(player1 == GameEngine.Players.playerX){
//                            dialog.dismiss();
//                        }else{
//                            player1 = GameEngine.Players.playerX;
//                            textViewPlayer1.setText("X");
//                            textViewPlayer1.setTextColor(Color.RED);
//                            imageButton1.setImageResource(R.drawable.x);
//                            textViewPlayer2.setText("O");
//                            textViewPlayer2.setTextColor(Color.GREEN);
//                            imageButton2.setImageResource(R.drawable.o);
//                            gameEngine.setImageButton1(imageButton1);
//                            gameEngine.setImageButton2(imageButton2);
//                            gameEngine.setPlayer1(player1);
//                            gameEngine.StartGame(player1);
//                        }
//                    }
//                    else{
//                        if(player1 == GameEngine.Players.playerO){
//                            dialog.dismiss();
//                        }else{
//                            player1 = GameEngine.Players.playerO;
//                            textViewPlayer1.setText("O");
//                            textViewPlayer1.setTextColor(Color.GREEN);
//                            imageButton1.setImageResource(R.drawable.o);
//                            textViewPlayer2.setText("X");
//                            textViewPlayer2.setTextColor(Color.RED);
//                            imageButton2.setImageResource(R.drawable.x);
//                            gameEngine.setImageButton1(imageButton1);
//                            gameEngine.setImageButton2(imageButton2);
//                            gameEngine.setPlayer1(player1);
//                            gameEngine.StartGame(player1);
//                        }
//                    }
//                    dialog.dismiss();
//                }
//            });
//            buttonCancel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//        }
//        else{
//            final AlertDialog.Builder builder = new AlertDialog.Builder(Playing_PlayWithFriend.this);
//            View v = getLayoutInflater().inflate(R.layout.change_icon, null);
//            final RadioButton radioButtonX = (RadioButton)v.findViewById(R.id.radioButtonX_ChangeIcon);
//            final RadioButton radioButtonY = (RadioButton)v.findViewById(R.id.radioButtonO_ChangeIcon);
//            final Button buttonOK = (Button)v.findViewById(R.id.buttonOK_ChangeIcon);
//            final Button buttonCancel = (Button)v.findViewById(R.id.buttonCancel_ChangeIcon);
//            builder.setView(v);
//            final AlertDialog dialog = builder.create();
//            dialog.show();
//            buttonOK.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(radioButtonX.isChecked()){
//                        if(player1 == GameEngine.Players.playerX){
//                            dialog.dismiss();
//                        }else{
//                            player1 = GameEngine.Players.playerO;
//                            textViewPlayer1.setText("O");
//                            textViewPlayer1.setTextColor(Color.GREEN);
//                            imageButton1.setImageResource(R.drawable.o);
//                            textViewPlayer2.setText("X");
//                            textViewPlayer2.setTextColor(Color.RED);
//                            imageButton2.setImageResource(R.drawable.x);
//                            gameEngine.setImageButton1(imageButton1);
//                            gameEngine.setImageButton2(imageButton2);
//                            gameEngine.setPlayer1(player1);
//                            gameEngine.StartGame(player1);
//                        }
//                    }
//                    else{
//                        if(player1 == GameEngine.Players.playerO){
//                            dialog.dismiss();
//                        }else{
//                            player1 = GameEngine.Players.playerX;
//                            textViewPlayer1.setText("X");
//                            textViewPlayer1.setTextColor(Color.RED);
//                            imageButton1.setImageResource(R.drawable.x);
//                            textViewPlayer2.setText("O");
//                            textViewPlayer2.setTextColor(Color.GREEN);
//                            imageButton2.setImageResource(R.drawable.o);
//                            gameEngine.setImageButton1(imageButton1);
//                            gameEngine.setImageButton2(imageButton2);
//                            gameEngine.setPlayer1(player1);
//                            gameEngine.StartGame(player1);
//                        }
//                    }
//                    dialog.dismiss();
//                }
//            });
//            buttonCancel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//        }
//    }

    private void Initialize() {
        textViewPlayer1 = (TextView)findViewById(R.id.textView9);
        textViewPlayer2 = (TextView)findViewById(R.id.textView10);
        buttonNewGame = (Button)findViewById(R.id.buttonNewGame_PlayWithFriend);
        buttonAgain = (Button)findViewById(R.id.buttonAgain_PlayWithFriend);
        buttonExit = (Button)findViewById(R.id.buttonExit_PlayWithFriend);
        imageButton1 = (ImageButton)findViewById(R.id.imageButton1);
        imageButton2 = (ImageButton)findViewById(R.id.imageButton2);
    }

}
