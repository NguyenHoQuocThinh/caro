package android.nguyenhoquocthinh.doancarop2p.Offline.HardMode;

import android.view.MotionEvent;
import android.view.View;

public class HardModeTouchListener implements View.OnTouchListener {
    private HardModeGameEngine hardModeGameEngine;

    private int startX, startY, stopX, stopY;

    public HardModeTouchListener(HardModeGameEngine hardModeGameEngine){
        this.hardModeGameEngine = hardModeGameEngine;
    }

    public boolean onTouch(View arg0, MotionEvent event){
        float sizeOfElement = hardModeGameEngine.getGridColumnSize() / hardModeGameEngine.getColumnNumber();

        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                startX = (int)(event.getX() / sizeOfElement);
                startY = (int) (event.getY() / sizeOfElement);
                break;
            case MotionEvent.ACTION_UP:
                stopX = (int)(event.getX() / sizeOfElement);
                stopY = (int) (event.getY() / sizeOfElement);
                break;
        }

        if(startY < hardModeGameEngine.getRowNumber() && startX < hardModeGameEngine.getColumnNumber()){
            if(startX == stopX && startY == stopY){
                if(hardModeGameEngine.playersGrid[startY][startX] == HardModeGameEngine.Players.None) {
                    hardModeGameEngine.addMark(startY, startX);
                    hardModeGameEngine.machineMoves();
                }
            }
        }

        return true;
    }
}
