package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.content.Context;
import android.nguyenhoquocthinh.doancarop2p.WifiP2P.EndDialog;
import android.util.Log;
import android.widget.ImageButton;

import java.util.ArrayList;

public class GameEngine {
    protected ImageButton imageButton1;

    protected Context context;
    protected final static int ROW_NUMBER = 21;
    protected final static int COLUMN_NUMBER = 15;
    protected float gridColumnSize;
    protected float gridRowSize;

    protected Players playersGrid[][];
    private Players p;
    private Players currentPlayer;

    protected enum Players {
        None, Players1, Players2
    }
    protected class point {
        int preI;
        int preJ;

        public point(int preI, int preJ) {
            this.preI = preI;
            this.preJ = preJ;
        }

        public int getPreI() {
            return preI;
        }

        public void setPreI(int preI) {
            this.preI = preI;
        }

        public int getPreJ() {
            return preJ;
        }

        public void setPreJ(int preJ) {
            this.preJ = preJ;
        }
    }

    private ArrayList<point> movement = new ArrayList<>();


    public GameEngine(Context context, Players p, ImageButton imageButton1) {
        playersGrid = new Players[ROW_NUMBER][COLUMN_NUMBER];

        this.imageButton1 = imageButton1;

        for (int i = 0; i < ROW_NUMBER; i++) {
            for (int j = 0; j < COLUMN_NUMBER; j++) {
                playersGrid[i][j] = Players.None;
            }
        }
        this.context = context;
        this.p = p;
        this.currentPlayer = p;
    }

    public Players checkEnd() {
        for (int i = 0; i < ROW_NUMBER; i++) {
            for (int j = 0; j < COLUMN_NUMBER; j++) {
                Players p = getPlayerAt(i, j);
                if (p != Players.None) {
                    if (WinCondition1(i, j, p) || WinCondition2(i, j, p) || WinCondition3(i, j, p) || WinCondition4(i, j, p))
                        return p;
                }
            }
        }
        return Players.None;
    }

    public void StartGame(Players player) {
        for (int i = 0; i < ROW_NUMBER; i++) {
            for (int j = 0; j < COLUMN_NUMBER; j++) {
                playersGrid[i][j] = Players.None;
            }
        }
    }

    public boolean isYourTurn(){
        if(this.p == this.currentPlayer)
            return true;
        return false;
    }

    private boolean WinCondition1(int i, int j, Players p) {
        int index = i + 1;
        int count = 0;
        if (p != Players.None) {
            while (index < ROW_NUMBER) {
                if (playersGrid[index][j] != p) {
                    return false;
                } else {
                    count++;
                }
                if (count == 4) {
                    if (i > 1 && index < ROW_NUMBER - 1) {
                        Players p1 = getPlayerAt(i - 1, j);
                        Players p2 = getPlayerAt(index + 1, j);
                        if (p1 == p2 && p1 != p && p1 != Players.None) {
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition2(int i, int j, Players p) {
        int index = j + 1;
        int count = 0;
        if (p != Players.None) {
            while (index < COLUMN_NUMBER) {
                if (playersGrid[i][index] != p) {
                    return false;
                } else {
                    count++;
                }
                if (count == 4) {
                    if (j > 1 && index < COLUMN_NUMBER - 1) {
                        Players p1 = getPlayerAt(i, j - 1);
                        Players p2 = getPlayerAt(i, index + 1);
                        if (p1 == p2 && p1 != p && p1 != Players.None) {
                            return false;
                        }
                    }
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    private boolean WinCondition3(int i, int j, Players p) {
        // 45 degrees line
        int iIndex = i + 1;
        int jIndex = j + 1;
        int count = 0;
        if (p != Players.None) {
            while (iIndex < ROW_NUMBER && jIndex < COLUMN_NUMBER) {
                if (playersGrid[iIndex][jIndex] != p) {
                    return false;
                } else {
                    count++;
                }
                if (count == 4) {
                    if (i > 0 && j > 0 && iIndex < ROW_NUMBER - 1 && jIndex < COLUMN_NUMBER - 1) {
                        Players p1 = getPlayerAt(i - 1, j - 1);
                        Players p2 = getPlayerAt(iIndex + 1, jIndex + 1);
                        if (p1 == p2 && p1 != p && p1 != Players.None) {
                            return false;
                        }
                    }
                    return true;
                }
                iIndex++;
                jIndex++;
            }
        }
        return false;
    }

    private boolean WinCondition4(int i, int j, Players p) {
        // 135 degrees line
        int iIndex = i - 1;
        int jIndex = j + 1;
        int count = 0;
        if (p != Players.None) {
            while (iIndex < ROW_NUMBER && jIndex < COLUMN_NUMBER && iIndex >= 0) {
                if (playersGrid[iIndex][jIndex] != p) {
                    return false;
                } else {
                    count++;
                }
                if (count == 4) {
                    if (i > 0 && j > 0 && i < ROW_NUMBER - 1 && iIndex > 0 && jIndex < COLUMN_NUMBER - 1) {
                        Players p1 = getPlayerAt(i + 1, j - 1);
                        Players p2 = getPlayerAt(iIndex - 1, jIndex + 1);
                        if (p1 == p2 && p1 != p && p1 != Players.None) {
                            return false;
                        }
                    }
                    return true;
                }
                iIndex--;
                jIndex++;
            }
        }
        return false;
    }

    public void addMark(int i, int j) {
        if (getPlayerAt(i, j) == Players.None) {
            movement.add(new point(i, j));
            playersGrid[i][j] = currentPlayer;
            SwitchPlayer();
        }

        Players winner = checkEnd();


        if (winner != Players.None) {
            String winnerText = "";
            if (winner == p) {
                Log.e("Caro", "Bạn thắng rồi!!!");
                winnerText = "Bạn thắng rồi!!!";
            } else {
                Log.e("Caro", "Thật tiêc, bạn thua rồi!");
                winnerText = "Thật tiếc, bạn thua rồi!";
            }
            new EndDialog(context, winnerText, this);
        }
    }

    public void SwitchPlayer() { // đổi icon khi đến lượt người chơi khác
        if(currentPlayer == Players.Players1){
            currentPlayer = Players.Players2;
        }else{
            currentPlayer = Players.Players1;
        }
    }

    public Players getPlayerAt(int i, int j){
        return playersGrid[i][j];
    }

    public float getGridRowSize() {
        return gridRowSize;
    }

    public void setGridRowSize(float gridRowSize) {
        this.gridRowSize = gridRowSize;
    }

    public float getGridColumnSize() {
        return gridColumnSize;
    }

    public void setGridColumnSize(float gridColumnSize) {
        this.gridColumnSize = gridColumnSize;
    }

    public int getRowNumber(){
        return ROW_NUMBER;
    }

    public int getColumnNumber(){
        return COLUMN_NUMBER;
    }

    public Players[][] getPlayersGrid(){
        return this.playersGrid;
    }

    public ImageButton getImageButton1() { return imageButton1; }

    public void setImageButton1(ImageButton imageButton1) { this.imageButton1 = imageButton1; }

    public Players getCurrentPlayer(){
        return this.currentPlayer;
    }

    public Players getP(){
        return this.p;
    }
}
