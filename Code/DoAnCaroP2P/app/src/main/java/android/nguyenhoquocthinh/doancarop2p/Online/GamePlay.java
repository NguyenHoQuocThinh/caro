package android.nguyenhoquocthinh.doancarop2p.Online;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Nguyen Ho Quoc Thinh on 04/25/2018.
 */

public class GamePlay extends AppCompatActivity {

    private FrameLayout frameLayout;
    private GameEngine gameEngine;
    private Socket socket;
    private Player player;
    private Button buttonExit;
    private ImageButton imageButtonX;
    private ImageButton imageButtonO;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_online_gameplay);

        socket = SocketHandler.getSocket();

        socket.on("QuitMiddleFeedback", QuitMiddleFeedback);

        Intent intent = getIntent();
        player = (Player) intent.getSerializableExtra("player");

        socket.emit("GoingOnline", player.getUsername());


        //Toast.makeText(this, player.getUsername() + "--" + player.getPassword(), Toast.LENGTH_SHORT).show();

        Initialize();

        gameEngine = new GameEngine(this, player, imageButtonX, imageButtonO);
        frameLayout = (FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.addView(new Graphic(this, gameEngine));


        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExitDialog();
            }
        });

    }

    public void ExitDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("THOÁT")
                .setMessage("Bạn có muốn thoát")
                .setPositiveButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Exit();
                        String mess = player.getUsername();
                        socket.emit("QuitMiddle", mess);
                    }
                })
                .setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog dialog = builder.create();
                        dialog.dismiss();
                    }
                }).setCancelable(false).show();
    }

    private void Initialize() {
        buttonExit = (Button)findViewById(R.id.buttonExit);
        imageButtonX = (ImageButton)findViewById(R.id.imageButtonX);
        imageButtonO = (ImageButton)findViewById(R.id.imageButtonO);
    }

    private Emitter.Listener QuitMiddleFeedback = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        String mess = object.getString("feedback");
                        Exit();
                       // Toast.makeText(GamePlay.this, mess, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void Exit() {
        Activity a = (Activity)this;
        a.finish();
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {

    }
}
