package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.view.View;

public class Graphics extends View {

    private GameEngine gameEngine;
    private Paint mPaint;
    private Bitmap bitmapX, bitmapO;
    private float sizeOfElement;

    public Graphics(Context context, GameEngine gameEngine){
        super(context);

        this.gameEngine = gameEngine;
        mPaint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(gameEngine.getGridRowSize() == 0){
            gameEngine.setGridRowSize(canvas.getHeight());
        }

        if(gameEngine.getGridColumnSize() == 0){
            gameEngine.setGridColumnSize(canvas.getWidth());

            sizeOfElement = gameEngine.getGridColumnSize() / gameEngine.getColumnNumber();
            loadAndCreateBitMap();
        }

        drawGrid(canvas);
        drawPlayers(canvas);

        invalidate();
    }

    private void drawPlayers(Canvas canvas) {
        for(int i = 0;i < gameEngine.getRowNumber();i ++){
            for(int j = 0; j < gameEngine.getColumnNumber();j ++){
                if(gameEngine.getPlayerAt(i, j) != GameEngine.Players.None){
                    Bitmap tempBitmap = null;
                    if (gameEngine.getPlayerAt(i, j) == GameEngine.Players.Players1) {
                        tempBitmap = bitmapX;
                    } else {
                        tempBitmap = bitmapO;
                    }
                    canvas.drawBitmap(tempBitmap, sizeOfElement * j, sizeOfElement * i, mPaint);
                }
            }
        }
    }

    private void loadAndCreateBitMap() {
        bitmapX = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
        bitmapO = BitmapFactory.decodeResource(this.getResources(), R.drawable.o);

        bitmapX = Bitmap.createScaledBitmap(bitmapX, (int)sizeOfElement, (int)sizeOfElement, true);
        bitmapO = Bitmap.createScaledBitmap(bitmapO, (int)sizeOfElement, (int)sizeOfElement, true);
    }

    private void drawGrid(Canvas canvas) {
        float rowSize = gameEngine.getGridRowSize()-210;
        float columnSize = gameEngine.getGridColumnSize();

        for(int i = 0;i < gameEngine.getRowNumber() + 1;i ++){
            canvas.drawLine(0,columnSize / gameEngine.getColumnNumber() * i,
                    columnSize, columnSize/ gameEngine.getColumnNumber()* i, mPaint);
        }

        for(int i = 0;i < gameEngine.getColumnNumber() + 1;i ++){
            canvas.drawLine(columnSize / gameEngine.getColumnNumber() * i, 0,
                    columnSize / gameEngine.getColumnNumber() * i, rowSize, mPaint);
        }
    }
}
