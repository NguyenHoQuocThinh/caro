package android.nguyenhoquocthinh.doancarop2p.Offline;

import android.util.Log;

import java.util.Random;

public class MachineEngine extends GameEngine{
    private static final int VERTICAL = 1;
    private static final int HORIZONTAL = 2;
    private static final int CROSSDOWN = 3;
    private static final int CROSSUP = 4;

    private static final int MIN_HEIGHT = 0;
    private static final int MAX_HEIGHT = 21;
    private static final int MIN_WIDTH= 0;
    private static final int MAX_WIDTH = 15;


    public MachineEngine(){

    }

    public int[] move(Players[][] playersGrid, Players enemy, Players p) {
        int pos[] = null;

        pos = machineHasFourRow(p, playersGrid);

        if(pos[0] != -1 && pos[1] != -1)
            return pos;
        else {
            for (int k = 4; k >= 1; k--) {//mỗi vòng lặp là để phát hiện tổng số các dấu do người chơi đã đánh,
                //chạy từ 4 đến 1 (VD: phát hiện ra 4 dấu liền nhau của người chơi để chặn)
                for (int i = 0; i < GameEngine.ROW_NUMBER; i++) {
                    for (int j = 0; j < GameEngine.COLUMN_NUMBER; j++) {
                        int rowMode = hasNumberOfRowIs(k, i, j, enemy, playersGrid);

                        if (rowMode != 0) {
                            pos = blockOneOfTheTwoHeads(rowMode, i, j, k, playersGrid, p);
                            if (pos[0] != -1 && pos[1] != -1)
                                return pos;
                        }
                    }
                }
            }
            if (pos[0] == -1 && pos[1] == -1) {
                for (int k = 4; k >= 1; k--) {
                    for (int i = 0; i < GameEngine.ROW_NUMBER; i++) {
                        for (int j = 0; j < GameEngine.COLUMN_NUMBER; j++) {
                            pos = secondChanceToBlockOneOfTheTwoHeads(i, j, k, playersGrid, p);
                            if (pos[0] != -1 && pos[1] != -1)
                                return pos;
                        }
                    }
                }
            }
            if (pos[0] == -1 && pos[1] == -1) {
                for (int i = 0; i < GameEngine.ROW_NUMBER; i++) {
                    for (int j = 0; j < GameEngine.COLUMN_NUMBER; j++) {
                        if (playersGrid[i][j] == Players.None) {
                            pos[0] = i;
                            pos[1] = j;
                            return pos;
                        }
                    }
                }
            }
        }
        return pos;
    }

    public int[] blockOneOfTheTwoHeads(int mode, int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = new int[2];
        boolean checked = false;

        switch(mode){
            case VERTICAL:
                if (i > MIN_HEIGHT) {
                    if (playerGrid[i - 1][j] == Players.None) {
                        pos[0] = i - 1;
                        pos[1] = j;
                        checked = true;
                    } else if (playerGrid[i - 1][j] == p) {
                        pos[0] = -1;
                        pos[1] = -1;
                        checked = false;
                    }
                }
                if (!checked) {
                    if (i < MAX_HEIGHT - (sequenceOfRow - 1)) {
                        if (playerGrid[i + sequenceOfRow][j] == Players.None) {
                            pos[0] = i + sequenceOfRow;
                            pos[1] = j;
                        } else if (playerGrid[i + sequenceOfRow][j] == p) {
                            pos[0] = -1;
                            pos[1] = -1;
                        }
                    }
                }
                break;
            case HORIZONTAL:
                if (j > MIN_WIDTH) {
                    if (playerGrid[i][j - 1] == Players.None) {
                        pos[0] = i;
                        pos[1] = j - 1;
                        checked = true;
                    } else if (playerGrid[i][j - 1] == p) {
                        pos[0] = -1;
                        pos[1] = -1;
                        checked = false;
                    }
                }
                if (!checked) {
                    if (j < MAX_WIDTH - (sequenceOfRow - 1)) {
                        if (playerGrid[i][j + sequenceOfRow] == Players.None) {
                            pos[0] = i;
                            pos[1] = j + sequenceOfRow;
                        } else if (playerGrid[i][j + sequenceOfRow] == p) {
                            pos[0] = -1;
                            pos[1] = -1;
                        }
                    }
                }
                break;
            case CROSSDOWN:
                if (i > MIN_HEIGHT && j > MIN_WIDTH) {
                    if (playerGrid[i - 1][j - 1] == Players.None) {
                        pos[0] = i - 1;
                        pos[1] = j - 1;
                        checked = true;
                    } else if (playerGrid[i - 1][j - 1] == p) {
                        pos[0] = -1;
                        pos[1] = -1;
                        checked = false;
                    }
                }
                if (!checked) {
                    if (i < MAX_HEIGHT - (sequenceOfRow - 1) && j < MAX_WIDTH - (sequenceOfRow - 1)) {
                        if (playerGrid[i + sequenceOfRow][j + sequenceOfRow] == Players.None) {
                            pos[0] = i + sequenceOfRow;
                            pos[1] = j + sequenceOfRow;
                        } else if (playerGrid[i + sequenceOfRow][j + sequenceOfRow] == p) {
                            pos[0] = -1;
                            pos[1] = -1;
                        }
                    }
                }
                break;
            case CROSSUP:
                if (i < MAX_HEIGHT && j > MIN_WIDTH) {
                    if (playerGrid[i + 1][j - 1] == Players.None) {
                        pos[0] = i + 1;
                        pos[1] = j - 1;
                        checked = true;
                    } else if (playerGrid[i + 1][j - 1] == p) {
                        pos[0] = -1;
                        pos[1] = -1;
                        checked = false;
                    }
                }
                if (!checked) {
                    if (i > MIN_HEIGHT + (sequenceOfRow - 1) && j < MAX_WIDTH - (sequenceOfRow - 1)) {
                        if (playerGrid[i - sequenceOfRow][j + sequenceOfRow] == Players.None) {
                            pos[0] = i - sequenceOfRow;
                            pos[1] = j + sequenceOfRow;
                        } else if (playerGrid[i - sequenceOfRow][j + sequenceOfRow] == p) {
                            pos[0] = -1;
                            pos[1] = -1;
                        }
                    }
                }
                break;
        }
        return pos;
    }

    public int[] secondChanceToBlockOneOfTheTwoHeads(int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = null;
        pos = addMarkInVertical(i, j, sequenceOfRow, playerGrid, p);
        if(pos[0] == -1 && pos[1] == -1)
            pos = addMarkInHorizontal(i, j, sequenceOfRow, playerGrid, p);
        if(pos[0] == -1 && pos[1] == -1)
            pos = addMarkInCrossDown(i, j, sequenceOfRow, playerGrid, p);
        if(pos[0] == -1 && pos[1] == -1)
            pos = addMarkInCrossUp(i, j, sequenceOfRow, playerGrid, p);

        return pos;
    }

    public int[] addMarkInVertical(int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = new int[2];
        boolean checked = false;
        if (i > MIN_HEIGHT) {
            if (playerGrid[i - 1][j] == Players.None) {
                pos[0] = i - 1;
                pos[1] = j;
                checked = true;
            } else if (playerGrid[i - 1][j] == p) {
                pos[0] = -1;
                pos[1] = -1;
            }
        }
        if (!checked) {
            if (i < MAX_HEIGHT - (sequenceOfRow - 1)) {
                if (playerGrid[i + sequenceOfRow][j] == Players.None) {
                    pos[0] = i + sequenceOfRow;
                    pos[1] = j;
                } else if (playerGrid[i + sequenceOfRow][j] == p) {
                    pos[0] = -1;
                    pos[1] = -1;
                }
            }
        }
        return pos;
    }

    public int[] addMarkInHorizontal(int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = new int [2];
        boolean checked = false;
        if (j > MIN_WIDTH) {
            if (playerGrid[i][j - 1] == Players.None) {
                pos[0] = i;
                pos[1] = j - 1;
                checked = true;
            } else if (playerGrid[i][j - 1] == p) {
                pos[0] = -1;
                pos[1] = -1;
            }
        }
        if (!checked) {
            if (j < MAX_WIDTH - (sequenceOfRow - 1)) {
                if (playerGrid[i][j + sequenceOfRow] == Players.None) {
                    pos[0] = i;
                    pos[1] = j + sequenceOfRow;
                } else if (playerGrid[i][j + sequenceOfRow] == p) {
                    pos[0] = -1;
                    pos[1] = -1;
                }
            }
        }
        return pos;
    }

    public int[] addMarkInCrossDown(int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = new int [2];
        boolean checked = false;

        if (i > MIN_HEIGHT && j > MIN_WIDTH) {
            if (playerGrid[i - 1][j - 1] == Players.None) {
                pos[0] = i - 1;
                pos[1] = j - 1;
                checked = true;
            } else if (playerGrid[i - 1][j - 1] == p) {
                pos[0] = -1;
                pos[1] = -1;
                checked = false;
            }
        }
        if (!checked) {
            if (i < MAX_HEIGHT - (sequenceOfRow - 1) && j < MAX_WIDTH - (sequenceOfRow - 1)) {
                if (playerGrid[i + sequenceOfRow][j + sequenceOfRow] == Players.None) {
                    pos[0] = i + sequenceOfRow;
                    pos[1] = j + sequenceOfRow;
                } else if (playerGrid[i + sequenceOfRow][j + sequenceOfRow] == p) {
                    pos[0] = -1;
                    pos[1] = -1;
                }
            }
        }
        return pos;
    }

    public int[] addMarkInCrossUp(int i, int j, int sequenceOfRow, Players[][] playerGrid, Players p){
        int pos[] = new int [2];
        boolean checked = false;

        if (i < MAX_HEIGHT && j > MIN_WIDTH) {
            if (playerGrid[i + 1][j - 1] == Players.None) {
                pos[0] = i + 1;
                pos[1] = j - 1;
                checked = true;
            } else if (playerGrid[i + 1][j - 1] == p) {
                pos[0] = -1;
                pos[1] = -1;
            }
        }
        if (!checked) {
            if (i > MIN_HEIGHT + (sequenceOfRow - 1) && j < MAX_WIDTH - (sequenceOfRow - 1)) {
                if (playerGrid[i - sequenceOfRow][j + sequenceOfRow] == Players.None) {
                    pos[0] = i - sequenceOfRow;
                    pos[1] = j + sequenceOfRow;
                } else if (playerGrid[i - sequenceOfRow][j + sequenceOfRow] == p) {
                    pos[0] = -1;
                    pos[1] = -1;
                }
            }
        }
        return pos;
    }

    public int hasNumberOfRowIs(int numberOfRow, int i, int j, Players player, Players[][] playersGrid){
        int index= i;
        int count = 0;

        //dọc
        if(player!=Players.None){
            while(index< GameEngine.ROW_NUMBER){
                if(playersGrid[index][j] != player || playersGrid[index][j] == Players.None){
                    break;
                }else{
                    count++;
                }
                if(count == numberOfRow){
                    if(index + 1 < ROW_NUMBER){
                        if(playersGrid[index + 1][j] == Players.None)
                            return VERTICAL;
                    }
                    if(index - numberOfRow >= 0) {
                        if (playersGrid[index - numberOfRow][j] == Players.None)
                            return VERTICAL;
                    }
                    else
                        break;
                }
                index++;
            }
        }

        //ngang
        index= j;
        count = 0;
        if(player!=Players.None){
            while(index<COLUMN_NUMBER){
                if(playersGrid[i][index] != player || playersGrid[i][index] == Players.None){
                    break;
                }else{
                    count++;
                }
                if(count == numberOfRow){
                    if(index + 1 < COLUMN_NUMBER){
                        if(playersGrid[i][index + 1] == Players.None)
                            return HORIZONTAL;
                    }
                    if(index - numberOfRow >= 0) {
                        if (playersGrid[i][index - numberOfRow] == Players.None)
                            return HORIZONTAL;
                    }
                    else
                        break;
                }
                index++;
            }
        }

        //chéo trái sang phải, trên xuống dưới
        int iIndex = i;
        int jIndex = j;
        count  = 0;
        if(player!=Players.None){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER){
                if(playersGrid[iIndex][jIndex] != player || playersGrid[iIndex][jIndex] == Players.None){
                    break;
                }else{
                    count++;
                }
                if(count == numberOfRow){
                    if(iIndex + 1 < ROW_NUMBER && jIndex + 1 < COLUMN_NUMBER){
                        if(playersGrid[iIndex + 1][jIndex + 1] == Players.None)
                            return CROSSDOWN;
                    }
                    if(iIndex - numberOfRow >= 0 && jIndex - numberOfRow >= 0) {
                        if (playersGrid[iIndex - numberOfRow][jIndex - numberOfRow] == Players.None)
                            return CROSSDOWN;
                    }
                    else
                        break;
                }
                iIndex++;
                jIndex++;
            }
        }

        //chéo trái sang phải, dưới lên trên
        iIndex = i;
        jIndex = j;
        count  = 0;
        if(player != Players.None){
            while(iIndex<ROW_NUMBER && jIndex<COLUMN_NUMBER&&iIndex>=0){
                if(playersGrid[iIndex][jIndex] != player || playersGrid[iIndex][jIndex] == Players.None){
                    break;
                }else{
                    count++;
                }
                if(count == numberOfRow){
                    if(iIndex + 1 < ROW_NUMBER && jIndex - 1 >= 0){
                        if(playersGrid[iIndex + 1][jIndex - 1] == Players.None)
                            return CROSSUP;
                    }
                    if(iIndex - numberOfRow >= 0 && jIndex + numberOfRow < COLUMN_NUMBER) {
                        if (playersGrid[iIndex - numberOfRow][jIndex + numberOfRow] == Players.None)
                            return CROSSUP;
                    }
                    else
                        break;
                }
                iIndex--;
                jIndex++;
            }
        }
        return 0;
    }

    public int[] machineHasFourRow(Players machine, Players[][] playersGrid){
        int pos[] = new int[2];
        pos[0] = -1;
        pos[1] = -1;
        for(int i = 0;i < ROW_NUMBER;i ++){
            for(int j = 0;j < COLUMN_NUMBER;j ++){
                pos = machineHasCoupleOfTwoAndTwoRow(i, j, machine, playersGrid);
                if(pos[0] != -1 && pos[1] != -1)
                    return pos;

                pos = machineHasCoupleOfThreeAndOneRow(i,j, machine, playersGrid);
                if(pos[0] != -1 && pos[1] != -1)
                    return pos;

                int rowMode = hasNumberOfRowIs(4, i, j, machine, playersGrid);
                switch(rowMode) {
                    case VERTICAL:
                        if(i < ROW_NUMBER - 4) {
                            if (playersGrid[i + 4][j] == Players.None) {
                                pos[0] = i + 4;
                                pos[1] = j;
                                return pos;
                            }
                        }
                        break;
                    case HORIZONTAL:
                        if(j < COLUMN_NUMBER - 4) {
                            if (playersGrid[i][j + 4] == Players.None) {
                                pos[0] = i;
                                pos[1] = j + 4;
                                return pos;
                            }
                        }
                        break;
                    case CROSSDOWN:
                        if(i < ROW_NUMBER - 4 && j < COLUMN_NUMBER - 4) {
                            if (playersGrid[i + 4][j + 4] == Players.None) {
                                pos[0] = i + 4;
                                pos[1] = j + 4;
                                return pos;
                            }
                        }
                        break;
                    case CROSSUP:
                        if(i > 3 && j < COLUMN_NUMBER - 4) {
                            if (playersGrid[i - 4][j + 4] == Players.None) {
                                pos[0] = i - 4;
                                pos[1] = j + 4;
                                return pos;
                            }
                        }
                        break;
                }
            }
        }
        return pos;
    }

    private int[] machineHasCoupleOfTwoAndTwoRow(int i, int j, Players machine, Players[][] playersGrid){
        int pos[] = new int[2];
        pos[0] = -1;
        pos[1] = -1;
        int twoRowMode = hasNumberOfRowIs(2, i, j, machine, playersGrid);
        if(twoRowMode != 0) {
            switch (twoRowMode) {
                case VERTICAL:
                    if (i < ROW_NUMBER - 4) {
                        if (playersGrid[i + 3][j] == machine && playersGrid[i + 4][j] == machine) {
                            if(playersGrid[i + 2][j] == Players.None){
                                pos[0] = i + 2;
                                pos[1] = j;
                                return pos;
                            }
                        }
                    }
                    break;
                case HORIZONTAL:
                    if (j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i][j + 3] == machine && playersGrid[i][j + 4] == machine) {
                            if (playersGrid[i][j + 2] == Players.None) {
                                pos[0] = i;
                                pos[1] = j + 2;
                                return pos;
                            }
                        }
                    }
                    break;
                case CROSSDOWN:
                    if (i < ROW_NUMBER - 4 && j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i + 3][j + 3] == machine && playersGrid[i + 4][j + 4] == machine) {
                            if(playersGrid[i + 2][j + 2] == Players.None){
                                pos[0] = i + 2;
                                pos[1] = j + 2;
                                return pos;
                            }
                        }
                    }
                    break;
                case CROSSUP:
                    if (i > 3 && j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i - 3][j + 3] == machine && playersGrid[i - 4][j + 4] == machine) {
                            if(playersGrid[i - 2][j + 2] == Players.None){
                                pos[0] = i - 2;
                                pos[1] = j + 2;
                                return pos;
                            }
                        }
                    }
                    break;
            }
        }
        return pos;
    }

    private int[] machineHasCoupleOfThreeAndOneRow(int i, int j, Players machine, Players[][] playersGrid){
        int pos[] = new int[2];
        pos[0] = -1;
        pos[1] = -1;
        int threeRowMode = hasNumberOfRowIs(3, i, j, machine, playersGrid);
        if(threeRowMode != 0) {
            switch (threeRowMode) {
                case VERTICAL:
                    if (i > 1) {
                        if (playersGrid[i - 2][j] == machine) {
                            if(playersGrid[i - 1][j] == Players.None){
                                pos[0] = i - 1;
                                pos[1] = j;
                                return pos;
                            }
                        }
                    }
                    if (i < ROW_NUMBER - 4) {
                        if (playersGrid[i + 4][j] == machine) {
                            if(playersGrid[i + 3][j] == Players.None){
                                pos[0] = i + 3;
                                pos[1] = j;
                                return pos;
                            }
                        }
                    }
                    break;
                case HORIZONTAL:
                    if (j > 1) {
                        if (playersGrid[i][j - 2] == machine) {
                            if(playersGrid[i][j - 1] == Players.None){
                                pos[0] = i;
                                pos[1] = j - 1;
                                return pos;
                            }
                        }
                    }
                    if (j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i][j + 4] == machine) {
                            if(playersGrid[i][j + 3] == Players.None){
                                pos[0] = i;
                                pos[1] = j + 3;
                                return pos;
                            }
                        }
                    }
                    break;
                case CROSSDOWN:
                    if (i > 1 && j > 1) {
                        if (playersGrid[i - 2][j - 2] == machine) {
                            if(playersGrid[i - 1][j - 1] == Players.None){
                                pos[0] = i - 1;
                                pos[1] = j - 1;
                                return pos;
                            }
                        }
                    }
                    if (i < ROW_NUMBER - 4 && j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i + 4][j + 4] == machine) {
                            if(playersGrid[i + 3][j + 3] == Players.None){
                                pos[0] = i + 3;
                                pos[1] = j + 3;
                                return pos;
                            }
                        }
                    }
                    break;
                case CROSSUP:
                    if (i < ROW_NUMBER - 2 && j > 1) {
                        if (playersGrid[i + 2][j - 2] == machine) {
                            if(playersGrid[i + 1][j - 1] == Players.None){
                                pos[0] = i + 1;
                                pos[1] = j - 1;
                                return pos;
                            }
                        }
                    }
                    if (i > 3 && j < COLUMN_NUMBER - 4) {
                        if (playersGrid[i - 4][j + 4] == machine) {
                            if(playersGrid[i - 3][j + 3] == Players.None){
                                pos[0] = i - 3;
                                pos[1] = j + 3;
                                return pos;
                            }
                        }
                    }
                    break;
            }
        }
        return pos;
    }
}
