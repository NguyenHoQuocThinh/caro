package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.content.Intent;
import android.graphics.Color;
import android.nguyenhoquocthinh.doancarop2p.R;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;

public class WifiP2PMainActivity extends AppCompatActivity{
    private WifiP2pASyncTask wifiP2pASyncTask;
    private TransferData transferData;
    private String host;
    String role1;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_p2p_main);

        Intent intent = getIntent();

        if(intent != null){
            String role = intent.getStringExtra("Role");
            role1=role;

            if(role.equals("Server")){
                int[] pos = new int[2];
                pos[0] = -1;
                pos[1] = -1;
                wifiP2pASyncTask = new WifiP2pASyncTask(WifiP2PMainActivity.this);
                wifiP2pASyncTask.execute(pos[0], pos[1]);
            }
            else if(role.equals("Client")){
                String hostAddr = intent.getStringExtra("Host");

                int[] pos = new int[2];
                pos[0] = -1;
                pos[1] = -1;
                transferData = new TransferData(WifiP2PMainActivity.this, 3333, hostAddr);
                transferData.execute(pos[0], pos[1]);
            }
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (role1.equals("Server")&&wifiP2pASyncTask.serverSocket!=null)
            try {
                wifiP2pASyncTask.serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else if (role1.equals("Client")&& transferData.getSocket()!=null) {
            try {
                transferData.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (role1.equals("Server")&&wifiP2pASyncTask.serverSocket!=null)
            try {
                wifiP2pASyncTask.serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else if (role1.equals("Client")&& transferData.getSocket()!=null) {
            try {
                transferData.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

