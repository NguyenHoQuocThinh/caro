package android.nguyenhoquocthinh.doancarop2p.WifiP2P;

import android.view.MotionEvent;
import android.view.View;

public class TouchListener implements View.OnTouchListener {

    private GameEngine gameEngine;
    private TransferData transferData = null;
    private WifiP2pASyncTask wifiP2pASyncTask = null;

    private int startX, startY, stopX, stopY;

    public TouchListener(GameEngine gameEngine, TransferData transferData){
        this.gameEngine = gameEngine;
        this.transferData = transferData;
    }

    public TouchListener(GameEngine gameEngine, WifiP2pASyncTask wifiP2pASyncTask){
        this.gameEngine = gameEngine;
        this.wifiP2pASyncTask = wifiP2pASyncTask;
    }

    public boolean onTouch(View arg0, MotionEvent event){
            float sizeOfElement = gameEngine.getGridColumnSize() / gameEngine.getColumnNumber();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startX = (int) (event.getX() / sizeOfElement);
                    startY = (int) (event.getY() / sizeOfElement);
                    break;
                case MotionEvent.ACTION_UP:
                    stopX = (int) (event.getX() / sizeOfElement);
                    stopY = (int) (event.getY() / sizeOfElement);
                    break;
            }

            if (startY < gameEngine.getRowNumber() && startX < gameEngine.getColumnNumber()) {
                if (startX == stopX && startY == stopY) {
                    if(gameEngine.isYourTurn() && gameEngine.getPlayerAt(startY, startX) == GameEngine.Players.None) {
                        if (transferData != null) {
                            gameEngine.addMark(startY, startX);
                            transferData.doInBackground(startY, startX);
                        } else if (wifiP2pASyncTask != null && wifiP2pASyncTask.isClientCome()) {
                            gameEngine.addMark(startY, startX);
                            wifiP2pASyncTask.doInBackground(startY, startX);
                        }
                    }
                }
            }
        return true;
    }
}
