package android.nguyenhoquocthinh.doancarop2p;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.w3c.dom.Text;

public class GameInfo extends AppCompatActivity {
    TextView infoText;
    private TextView textView_GroupInfo;
    private TextView textView_GameInfo;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_info);
        Initialize();
        String groupInfo = "Thành viên nhóm:\n\n" +
                "1512521 - Nguyễn Duy Thắng\n" +
                "1512544 - Nguyễn Hồ Quốc Thịnh\n" +
                "1512562 - Nguyễn Hữu Thương\n" +
                "1512640 - Trương Quang Tuấn";
        String gameInfo = "Luật chơi:\n\n" +
                "- Người chơi phải có đủ 5 hình liên tiếp nhau trên bàn cờ ở hàng dọc hoặc hàng ngang hoặc đường chéo thì mới thắng game\n\n" +
                "- Nếu 5 hình liên tiếp nhau của người mà bị chặn 2 đầu bởi hình của đối thủ thì sẽ không thể thắng game\n";
        this.textView_GroupInfo.setText(groupInfo);
        this.textView_GameInfo.setText(gameInfo);
    }

    private void Initialize() {
        this.textView_GroupInfo = (TextView)findViewById(R.id.textView_GroupInfo);
        this.textView_GameInfo = (TextView)findViewById(R.id.textView_GameInfo);
    }
}
